## Pokedex REST Sample

This application contains mini application to view entities in anonymous mode and in secured mode.<br>
<br>
This application developed using Spring MVC (not Spring Boot), Hibernate and H2 as persistence provider.<br>
The database automatically populated by reading pokemon.json file<br>
<br>*Note: There is no front end solution*
### The API
#### The Pokedex
Pokedex is a collection of REST API to read pokemon information.<br>Below are REST endpoint provided by the application.
- Get list of pokemon

```
GET /rest/pokemon/
```
- Get a pokemon which ID is **PIKACHU**

```
GET /rest/pokemon/PIKACHU
```
- Get list of pokemon that have type **POKEMON_TYPE_GRASS**

```
GET /rest/pokemon/type/POKEMON_TYPE_GRASS
```
- Get filtered list of pokemon where<br>
   . type is **POKEMON_TYPE_STEEL**,<br> 
   . with statistics: at least 0 baseAttack , at least 200 baseDefense, at least 50 baseStamina,<br> 
   . and sorted by name ascending. <br>

```
GET /rest/pokemon/type/POKEMON_TYPE_GRASS \
 --header "Content-Type: application/json" \
 --data '{ \
           "type":"POKEMON_TYPE_STEEL",
           "statsFilter": {\
              "baseAttack": 0,
              "baseDefense": 200,
              "baseStamina": 50
           }, \
           "sorts": [\
              { \
                 "field": "name",
                 "ascending": false
              } \
           ], \
         }' \
           
```
*Note: All the fields for filter are optional.*

#### The Pokemon Type
Below are REST endpoint provided by the application to retrieve list of Pokemon types.
- Get list of Pokemon types.

```
GET /rest/type/
```
- Get a pokemon type with ID **POKEMON_TYPE_STEEL**

```
GET /rest/type/POKEMON_TYPE_STEEL
```

##TODO
- Create pagination for Pokedex
- Create entity structure for evolution
- Implement security login

### Build the Project
```
mvn clean install
```
