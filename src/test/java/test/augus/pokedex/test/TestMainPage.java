package test.augus.pokedex.test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import test.augus.pokedex.controller.RootIndexController;
import test.augus.pokedex.test.config.TestConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TestMainPage {
    @Autowired
    private RootIndexController indexController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testWebIsRunning() throws Exception {
        mockMvc //
                .perform(get("/")) //
                .andExpect(status().isOk()) //
                .andExpect(content().string(containsString(RootIndexController.WELCOME_MESSAGE)));
    }

    @Test
    public void testWebIsRunning2() {
        String result = indexController.greeting();
        assertEquals(RootIndexController.WELCOME_MESSAGE, result);

    }
}
