package test.augus.pokedex.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import test.augus.pokedex.controller.PokemonTypeController;
import test.augus.pokedex.dto.master.BaseTypeDTO;
import test.augus.pokedex.test.config.TestConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TestPokemonTypeRESTService {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    final String BASE_URL = PokemonTypeController.BASE_URL;

    @Test
    public void testGetAll() throws Exception {
        MvcResult result = mockMvc.perform(get(BASE_URL).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<BaseTypeDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<BaseTypeDTO>>() {
            });
            Assert.assertEquals("Expect pokedex contains x element", 18, pokedex.size());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testGetInvalid() throws Exception {
        String id = "INVALID";
        mockMvc.perform(get(BASE_URL + "id/" + id).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isNoContent());
    }

    @Test
    public void testGetValidOne() throws Exception {
        String id = "POKEMON_TYPE_ELECTRIC";
        MvcResult result = mockMvc.perform(get(BASE_URL + "id/" + id).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            BaseTypeDTO pokemonType = objectMapper.readValue(contentAsString, BaseTypeDTO.class);
            assertEquals("Expect id", id, pokemonType.getId());
            assertEquals("Expect name ", "Electric", pokemonType.getName());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

}
