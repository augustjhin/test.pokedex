package test.augus.pokedex.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import test.augus.pokedex.controller.PokedexController;
import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.dto.PokemonParamDTO;
import test.augus.pokedex.dto.PokemonParamDTO.SortDTO;
import test.augus.pokedex.dto.master.BaseTypeDTO;
import test.augus.pokedex.dto.pokemon.attributes.PokemonStatisticDTO;
import test.augus.pokedex.test.config.TestConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TestPokedexRESTService {

    private static final String BASE_URL = PokedexController.BASE_URL;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private boolean compareStats(PokemonStatisticDTO expected, PokemonStatisticDTO actual) {
        if (expected.getBaseAttack() > actual.getBaseAttack()) {
            return false;
        }
        if (expected.getBaseDefense() > actual.getBaseDefense()) {
            return false;
        }
        if (expected.getBaseStamina() > actual.getBaseStamina()) {
            return false;
        }
        return true;
    }

    private String prepareFilterBySort() throws JsonProcessingException {
        PokemonParamDTO param = new PokemonParamDTO();
        param.setSorts(new SortDTO("dex", Boolean.FALSE));
        return objectMapper.writeValueAsString(param);
    }

    private String prepareFilterByStats(PokemonStatisticDTO stats) throws JsonProcessingException {
        PokemonParamDTO param = new PokemonParamDTO();
        param.setStatsFilter(stats);
        return objectMapper.writeValueAsString(param);
    }

    private String prepareFilterByType(String type) throws JsonProcessingException {
        PokemonParamDTO param = new PokemonParamDTO();
        param.setType(type);
        return objectMapper.writeValueAsString(param);
    }

    @Test
    public void testFilterByStatistic() throws Exception {
        PokemonStatisticDTO stats = new PokemonStatisticDTO(null, 200, 200, 0);
        String inputJson = prepareFilterByStats(stats);
        MockHttpServletRequestBuilder getWithFilter = get(BASE_URL) //
                .accept(MediaType.APPLICATION_JSON) //
                .characterEncoding(StandardCharsets.UTF_8.name()) //
                .contentType(MediaType.APPLICATION_JSON) //
                .content(inputJson);
        MvcResult result = mockMvc.perform(getWithFilter) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<PokemonDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<PokemonDTO>>() {
            });
            Assert.assertEquals("Expect pokedex contains x element", 48, pokedex.size());
            for (PokemonDTO pokemon : pokedex) {
                assertTrue("Expect statistic", compareStats(stats, pokemon.getStats()));
            }
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testFilterByType() throws Exception {
        String type = "POKEMON_TYPE_ELECTRIC";
        String inputJson = prepareFilterByType(type);
        MockHttpServletRequestBuilder getWithFilter = get(BASE_URL) //
                .accept(MediaType.APPLICATION_JSON) //
                .characterEncoding(StandardCharsets.UTF_8.name()) //
                .contentType(MediaType.APPLICATION_JSON) //
                .content(inputJson);
        MvcResult result = mockMvc.perform(getWithFilter) //
                .andExpect(status().isOk()) //
                // .andDo(MockMvcResultHandlers.print()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<PokemonDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<PokemonDTO>>() {
            });
            Assert.assertEquals("Expect pokedex contains x element", 38, pokedex.size());
            for (PokemonDTO pokemon : pokedex) {
                assertTrue("Expect type", Arrays.stream(pokemon.getTypes()).map(BaseTypeDTO::getId).anyMatch(t -> type.equals(t)));
            }
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testGetAll() throws Exception {
        MvcResult result = mockMvc.perform(get(BASE_URL).accept("application/json")) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<PokemonDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<PokemonDTO>>() {
            });
            Assert.assertEquals("Expect pokedex", 554, pokedex.size());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testGetByType() throws Exception {
        MvcResult result = mockMvc.perform(get(BASE_URL + "/type/POKEMON_TYPE_ELECTRIC").accept("application/json")) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<PokemonDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<PokemonDTO>>() {
            });
            Assert.assertEquals("Expect pokedex", 38, pokedex.size());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testGetInvalid() throws Exception {
        String id = "INVALID";
        mockMvc.perform(get(BASE_URL + "id/" + id).accept("application/json")) //
                .andExpect(status().isNoContent());
    }

    @Test
    public void testGetValidOne() throws Exception {
        String id = "PIKACHU";
        MvcResult result = mockMvc.perform(get(BASE_URL + "id/" + id).accept("application/json")) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            PokemonDTO pokemon = objectMapper.readValue(contentAsString, PokemonDTO.class);
            assertEquals("Expect name PIKACHU", id, pokemon.getName());
            assertEquals("Expect max CP 938", 938, pokemon.getMaxCP().intValue());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testSorted() throws Exception {
        String inputJson = prepareFilterBySort();
        MockHttpServletRequestBuilder getWithFilter = get(BASE_URL) //
                .accept(MediaType.APPLICATION_JSON) //
                .characterEncoding(StandardCharsets.UTF_8.name()) //
                .contentType(MediaType.APPLICATION_JSON) //
                .content(inputJson);
        MvcResult result = mockMvc.perform(getWithFilter) //
                .andExpect(status().isOk()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<PokemonDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<PokemonDTO>>() {
            });
            Assert.assertEquals("Expect pokedex", 554, pokedex.size());
            Assert.assertTrue("Expect pokedex sorted by dex", pokedex.get(0).getDex().compareTo(pokedex.get(1).getDex()) > 0);
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

}
