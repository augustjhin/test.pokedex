package test.augus.pokedex.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import test.augus.pokedex.controller.PokemonUserController;
import test.augus.pokedex.dto.BasePokemonDTO;
import test.augus.pokedex.dto.PokemonUserDTO;
import test.augus.pokedex.dto.pokemon.attributes.PokemonStatisticDTO;
import test.augus.pokedex.model.dao.PokemonUserDAO;
import test.augus.pokedex.test.config.TestConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TestPokemonUserRESTService {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    final String BASE_URL = PokemonUserController.BASE_URL;

    @Autowired
    PokemonUserDAO dao;

    private PokemonUserDTO createNewEntity() {
        PokemonUserDTO entity = newEntity();
        return dao.insert(entity);
    }

    private PokemonUserDTO newEntity() {
        PokemonUserDTO entity = new PokemonUserDTO();
        entity.setPokemon(new BasePokemonDTO("CHARMANDER", "Charmander"));
        entity.setCurrentStats(new PokemonStatisticDTO(null, 100, 150, 175));
        entity.setUserId("user1@test.com");
        return entity;
    }

    @After
    public void tearDown() {
        dao.deleteAll();
    }

    @Test
    public void testDeleteInvalid() throws Exception {
        String id = "INVALID";
        mockMvc.perform(delete(BASE_URL + "id/" + id).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteValid() throws Exception {
        PokemonUserDTO entity = createNewEntity();

        String id = entity.getId();
        mockMvc.perform(delete(BASE_URL + "id/" + id).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk());
        Assert.assertNull("Expect deleted", dao.get(id));
    }

    @Test
    public void testGetAll() throws Exception {
        createNewEntity();

        MvcResult result = mockMvc.perform(get(BASE_URL).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isOk()) //
                // .andDo(MockMvcResultHandlers.print()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            List<PokemonUserDTO> pokedex = objectMapper.readValue(contentAsString, new TypeReference<List<PokemonUserDTO>>() {
            });
            Assert.assertEquals("Expect pokedex contains 1 element", 1, pokedex.size());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testGetInvalid() throws Exception {
        String id = "INVALID";
        mockMvc.perform(get(BASE_URL + "id/" + id).accept(MediaType.APPLICATION_JSON)) //
                .andExpect(status().isNoContent());
    }

    @Test
    public void testInsertNew() throws Exception {
        PokemonUserDTO newEntity = newEntity();
        String inputJson = objectMapper.writeValueAsString(newEntity);

        MockHttpServletRequestBuilder postRequestBuilder = post(BASE_URL) //
                .accept(MediaType.APPLICATION_JSON) //
                .characterEncoding(StandardCharsets.UTF_8.name()) //
                .contentType(MediaType.APPLICATION_JSON) //
                .content(inputJson);
        MvcResult result = mockMvc.perform(postRequestBuilder) //
                .andExpect(status().isCreated()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            PokemonUserDTO savedEntity = objectMapper.readValue(contentAsString, PokemonUserDTO.class);
            assertNotNull("Expect has returned entity", savedEntity);
            assertNotNull("Expect returned entity id", savedEntity.getId());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

    @Test
    public void testUpdateExisting() throws Exception {
        PokemonUserDTO newEntity = newEntity();
        PokemonUserDTO updatedEntity = dao.insert(newEntity);

        updatedEntity.setPokemon(new BasePokemonDTO("MELMETAL", ""));
        updatedEntity.setCurrentCP(9999);
        PokemonStatisticDTO currentStats = updatedEntity.getCurrentStats();
        currentStats.setBaseAttack(999);
        String inputJson = objectMapper.writeValueAsString(updatedEntity);

        MockHttpServletRequestBuilder postRequestBuilder = put(BASE_URL) //
                .accept(MediaType.APPLICATION_JSON) //
                .characterEncoding(StandardCharsets.UTF_8.name()) //
                .contentType(MediaType.APPLICATION_JSON) //
                .content(inputJson);
        MvcResult result = mockMvc.perform(postRequestBuilder) //
                .andExpect(status().isCreated()) //
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        try {
            PokemonUserDTO savedEntity = objectMapper.readValue(contentAsString, PokemonUserDTO.class);
            assertNotNull("Expect has returned entity", savedEntity);
            assertEquals("Expect returned entity id", updatedEntity.getId(), savedEntity.getId());
            assertEquals("Expect returned pokemon type", newEntity.getPokemon(), savedEntity.getPokemon());
            assertNotEquals("Expect lower CP", updatedEntity.getCurrentCP(), savedEntity.getCurrentCP());
            assertEquals("Expect returned statistic", 999, savedEntity.getCurrentStats().getBaseAttack());
        } catch (Exception e) {
            assertFalse("Failed to parse return object", true);
            throw e;
        }
    }

}
