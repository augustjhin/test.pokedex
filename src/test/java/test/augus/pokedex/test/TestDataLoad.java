package test.augus.pokedex.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.model.dao.PokemonDAO;
import test.augus.pokedex.test.config.TestConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TestDataLoad {
    @Autowired
    private PokemonDAO dao;

    @Test
    public void testPokedexLoaded() throws Exception {
        List<PokemonDTO> pokedex = dao.getAll();
        Assert.assertFalse("Expect pokedex loaded", pokedex.isEmpty());
        Assert.assertEquals("Expect pokedex contains x element", 554, pokedex.size());
    }

}
