package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.master.CinematicMove;

public interface CinematicMoveJpaRepository extends JpaRepository<CinematicMove, String> {

}
