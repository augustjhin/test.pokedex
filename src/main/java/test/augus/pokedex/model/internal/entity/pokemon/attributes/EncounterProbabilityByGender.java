package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "ENCOUNTER_GENDER_PROBABILTY")
public class EncounterProbabilityByGender implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(precision = 10, scale = 3)
    private BigDecimal malePercent;
    @Column(precision = 10, scale = 3)
    private BigDecimal femalePercent;

    public EncounterProbabilityByGender() {
    }

    public EncounterProbabilityByGender(String id, BigDecimal malePercent, BigDecimal femalePercent) {
        this.id = id;
        this.malePercent = malePercent;
        this.femalePercent = femalePercent;
    }

    public BigDecimal getFemalePercent() {
        return femalePercent;
    }

    @Override
    public String getId() {
        return id;
    }

    public BigDecimal getMalePercent() {
        return malePercent;
    }

    public void setFemalePercent(BigDecimal femalePercent) {
        this.femalePercent = femalePercent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMalePercent(BigDecimal malePercent) {
        this.malePercent = malePercent;
    }
}
