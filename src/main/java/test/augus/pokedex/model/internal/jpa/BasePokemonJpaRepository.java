package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.BasePokemon;

public interface BasePokemonJpaRepository extends JpaRepository<BasePokemon, String> {

}