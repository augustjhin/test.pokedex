package test.augus.pokedex.model.internal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.pokemon.attributes.CurrentPokemonStatistic;

@Entity
@Table(name = "USER_POKEMON")
public class PokemonUser implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "USER_ID", nullable = false, updatable = false)
    User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "POKEMON_ID", nullable = false, updatable = false)
    BasePokemon pokemon;

    private String customName;
    private Integer currentCP;
    private java.sql.Timestamp caughtDate;
    private Boolean favourited;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CurrentPokemonStatistic currentStats;

    public java.sql.Timestamp getCaughtDate() {
        return caughtDate;
    }

    public Integer getCurrentCP() {
        return currentCP;
    }

    public CurrentPokemonStatistic getCurrentStats() {
        return currentStats;
    }

    public String getCustomName() {
        return customName;
    }

    public Boolean getFavourited() {
        return favourited == null ? Boolean.FALSE : favourited;
    }

    @Override
    public String getId() {
        return id;
    }

    public BasePokemon getPokemon() {
        return pokemon;
    }

    public User getUser() {
        return user;
    }

    public void setCaughtDate(java.sql.Timestamp caughtDate) {
        this.caughtDate = caughtDate;
    }

    public void setCurrentCP(Integer currentCP) {
        this.currentCP = currentCP;
    }

    public void setCurrentStats(CurrentPokemonStatistic currentStats) {
        this.currentStats = currentStats;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public void setFavourited(Boolean favourited) {
        this.favourited = favourited;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPokemon(BasePokemon pokemon) {
        this.pokemon = pokemon;
        this.customName = pokemon == null ? null : pokemon.getName();
    }

    public void setUser(User user) {
        this.user = user;
    }
}
