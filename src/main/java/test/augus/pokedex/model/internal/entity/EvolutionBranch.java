package test.augus.pokedex.model.internal.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.master.EvolutionItem;

@Entity
@Table(name = "EVOLUTION_BRANCH")
public class EvolutionBranch implements IBaseEntity {
    @Id
    private String id;

    @OneToOne
    private BasePokemon pokemon;

    @OneToOne
    private EvolutionItem costToEvolve;

    @OneToMany
    private List<EvolutionBranch> futureBranch;

    public EvolutionBranch() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
