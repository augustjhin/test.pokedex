package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.master.QuickMove;

public interface QuickMoveJpaRepository extends JpaRepository<QuickMove, String> {

}
