package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.User;

public interface UserJpaRepository extends JpaRepository<User, String> {

}
