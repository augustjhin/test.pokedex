package test.augus.pokedex.model.internal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.master.EvolutionItem;

@Entity
@Table(name = "EVOLUTION_COST")
public class EvolutionCost implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String evolutionCostId;
    private Integer candyCost;
    private Integer kmBuddyDistance;

    @OneToOne
    private EvolutionItem evolutionItem;

    public Integer getCandyCost() {
        return candyCost;
    }

    public String getEvolutionCostId() {
        return evolutionCostId;
    }

    public EvolutionItem getEvolutionItem() {
        return evolutionItem;
    }

    @Override
    public String getId() {
        return evolutionCostId;
    }

    public Integer getKmBuddyDistance() {
        return kmBuddyDistance;
    }

    public void setCandyCost(Integer candyCost) {
        this.candyCost = candyCost;
    }

    public void setEvolutionCostId(String evolutionCostId) {
        this.evolutionCostId = evolutionCostId;
    }

    public void setEvolutionItem(EvolutionItem evolutionItem) {
        this.evolutionItem = evolutionItem;
    }

    public void setKmBuddyDistance(Integer kmBuddyDistance) {
        this.kmBuddyDistance = kmBuddyDistance;
    }
}
