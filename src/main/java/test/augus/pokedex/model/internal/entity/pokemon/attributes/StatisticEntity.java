package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

public interface StatisticEntity extends IBaseEntity {

    public int getBaseAttack();

    public int getBaseDefense();

    public int getBaseStamina();

    public void setBaseAttack(int baseAttack);

    public void setBaseDefense(int baseDefense);

    public void setBaseStamina(int baseStamina);

    public void setId(String id);

}