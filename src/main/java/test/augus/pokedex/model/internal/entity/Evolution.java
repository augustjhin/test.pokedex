package test.augus.pokedex.model.internal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.master.EvolutionItem;

@Entity
@Table(name = "EVOLUTION")
public class Evolution implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    // futureBranches?: FutureBranch2[];
    // pastBranch?: PastBranch;
    @OneToOne
    private EvolutionItem costToEvolve;

    public EvolutionItem getCostToEvolve() {
        return costToEvolve;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setCostToEvolve(EvolutionItem costToEvolve) {
        this.costToEvolve = costToEvolve;
    }

    public void setId(String id) {
        this.id = id;
    }
}
