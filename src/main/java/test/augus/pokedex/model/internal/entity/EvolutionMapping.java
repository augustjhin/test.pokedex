package test.augus.pokedex.model.internal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "EVOLUTION_MAPPING")
public class EvolutionMapping implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    @OneToOne
    private BasePokemon original;

    @OneToOne
    private BasePokemon future;

    @OneToOne
    private EvolutionCost costToEvolve;

    public EvolutionCost getCostToEvolve() {
        return costToEvolve;
    }

    public BasePokemon getFuture() {
        return future;
    }

    @Override
    public String getId() {
        return id;
    }

    public BasePokemon getOriginal() {
        return original;
    }

    public void setCostToEvolve(EvolutionCost costToEvolve) {
        this.costToEvolve = costToEvolve;
    }

    public void setFuture(BasePokemon future) {
        this.future = future;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOriginal(BasePokemon original) {
        this.original = original;
    }
}
