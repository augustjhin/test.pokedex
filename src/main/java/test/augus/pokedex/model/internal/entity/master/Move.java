package test.augus.pokedex.model.internal.entity.master;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "MOVES")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "type")
public class Move implements IBaseEntity {

    @Id
    @Column(name = "ID")
    protected String id;
    protected String name;

    public Move() {
    }

    public Move(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Moves [name=" + name + ", id=" + id + "]";
    }

}