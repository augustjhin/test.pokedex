package test.augus.pokedex.model.internal.entity.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "RARITY")
public class Rarity implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;
    private String name;

    public Rarity() {
    }

    public Rarity(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Rarity) {
            Rarity anotherObj = (Rarity) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Rarity [name=" + name + ", id=" + id + "]";
    }
}
