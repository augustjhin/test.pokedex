package test.augus.pokedex.model.internal.entity.master;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import test.augus.pokedex.constant.PokemonConstant;

@Entity
@DiscriminatorValue(PokemonConstant.MOVEMENT_QUICK)
public class QuickMove extends Move {
    public QuickMove() {
    }

    public QuickMove(String id, String name) {
        super(id, name);
    }
}
