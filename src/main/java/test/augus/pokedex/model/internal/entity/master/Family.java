package test.augus.pokedex.model.internal.entity.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "FAMILY")
public class Family implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;
    private String name;

    public Family() {
    }

    public Family(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Family [name=" + name + ", id=" + id + "]";
    }
}
