package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.pokemon.attributes.EncounterMovementType;

public interface EncounterMovementTypeJpaRepository extends JpaRepository<EncounterMovementType, String> {

}
