package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "POKEMON_FORMS")
public class PokemonForm implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    private String pokemonId;
    private String formId;
    private String name;

    public PokemonForm() {
    }

    public PokemonForm(String pokemonId, String formId, String name) {
        this.pokemonId = pokemonId;
        this.formId = formId;
        this.name = name;
        this.id = pokemonId + "|" + formId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof PokemonForm) {
            PokemonForm anotherObj = (PokemonForm) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    public String getFormId() {
        return formId;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPokemonId() {
        return pokemonId;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPokemonId(String pokemonId) {
        this.pokemonId = pokemonId;
    }

    @Override
    public String toString() {
        return "PokemonForm [name=" + name + ", id=" + formId + "]";
    }
}
