package test.augus.pokedex.model.internal.entity.master;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "EVOLUTION_ITEM")
public class EvolutionItem implements IBaseEntity {
    @Id
    private String id;
    private String name;

    public EvolutionItem() {
    }

    public EvolutionItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EvolutionItem [name=" + name + ", id=" + id + "]";
    }
}
