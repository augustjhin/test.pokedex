package test.augus.pokedex.model.internal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "BASE_POKEMON")
public class BasePokemon implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;
    private String name;
    private Integer dex;
    private Integer maxCP;

    public BasePokemon() {
    }

    public BasePokemon(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BasePokemon) {
            BasePokemon anotherObj = (BasePokemon) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    public Integer getDex() {
        return dex;
    }

    @Override
    public String getId() {
        return id;
    }

    public Integer getMaxCP() {
        return maxCP;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setDex(Integer dex) {
        this.dex = dex;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMaxCP(Integer maxCP) {
        this.maxCP = maxCP;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pokemon [name=" + name + ", id=" + id + "]";
    }
}
