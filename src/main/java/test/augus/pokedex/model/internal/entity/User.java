package test.augus.pokedex.model.internal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User implements IBaseEntity {
    @Id
    @Column(name = "ID")
    String id;
    String name;

    String password;
    java.sql.Timestamp joinDate;

    public User() {
    }

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof User) {
            User anotherObj = (User) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public java.sql.Timestamp getJoinDate() {
        return joinDate;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public boolean isPasswordMatch(String password) {
        if (this.password == null || password == null) {
            return false;
        }
        return this.password.equals(password);
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setJoinDate(java.sql.Timestamp joinDate) {
        this.joinDate = joinDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", id=" + id + "]";
    }
}
