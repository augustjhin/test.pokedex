package test.augus.pokedex.model.internal.entity;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import test.augus.pokedex.model.internal.entity.master.BaseType;
import test.augus.pokedex.model.internal.entity.master.BuddySize;
import test.augus.pokedex.model.internal.entity.master.CinematicMove;
import test.augus.pokedex.model.internal.entity.master.Family;
import test.augus.pokedex.model.internal.entity.master.QuickMove;
import test.augus.pokedex.model.internal.entity.master.Rarity;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.AnimationTime;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.CameraSetup;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.Encounter;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.PokemonForm;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.PokemonStatistic;

/**
 * Main Pokemon entity.
 *
 * @author augustoportjhin
 *
 */

@Entity
@Table(name = "POKEMON")
public class Pokemon extends BasePokemon {

    // measurement
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal modelHeight;
    private BigDecimal modelScale;
    private Integer kmBuddyDistance;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "pokemon_id")
    private List<AnimationTime> animationTime;

    @OneToOne
    private BuddySize buddySize;
    @OneToOne
    private Family family;
    @OneToOne
    private Rarity rarity;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private PokemonStatistic stats;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CameraSetup camera;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Encounter encounter;

    @ManyToMany(cascade = { CascadeType.REMOVE })
    @JoinTable(name = "POKEMON_CINEMATIC_MOVES", joinColumns = { @JoinColumn(name = "pokemon_id") }, inverseJoinColumns = { @JoinColumn(name = "move_id") })
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<CinematicMove> cinematicMoves;

    @ManyToMany(cascade = { CascadeType.REMOVE })
    @JoinTable(name = "POKEMON_QUICK_MOVES", joinColumns = { @JoinColumn(name = "pokemon_id") }, inverseJoinColumns = { @JoinColumn(name = "move_id") })
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<QuickMove> quickMoves;

    @ManyToMany(cascade = { CascadeType.REMOVE })
    @JoinTable(name = "POKEMON_BASE_TYPE", joinColumns = { @JoinColumn(name = "pokemon_id") }, inverseJoinColumns = { @JoinColumn(name = "type_id") })
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BaseType> types;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<PokemonForm> forms;

    /**
     * Default constructor
     */
    public Pokemon() { // NOSONAR

    }
    // evolution: Evolution;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Pokemon) {
            Pokemon anotherObj = (Pokemon) obj;
            return this.getId().equals(anotherObj.getId());
        }
        return false;
    }

    public List<AnimationTime> getAnimationTime() {
        return animationTime == null ? Collections.emptyList() : animationTime;
    }

    public BuddySize getBuddySize() {
        return buddySize;
    }

    public CameraSetup getCamera() {
        return camera;
    }

    public List<CinematicMove> getCinematicMoves() {
        return cinematicMoves == null ? Collections.emptyList() : cinematicMoves;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public Family getFamily() {
        return family;
    }

    public List<PokemonForm> getForms() {
        return forms;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public Integer getKmBuddyDistance() {
        return kmBuddyDistance;
    }

    public BigDecimal getModelHeight() {
        return modelHeight;
    }

    public BigDecimal getModelScale() {
        return modelScale;
    }

    public List<QuickMove> getQuickMoves() {
        return quickMoves == null ? Collections.emptyList() : quickMoves;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public PokemonStatistic getStats() {
        return stats;
    }

    public List<BaseType> getTypes() {
        return types;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void setAnimationTime(List<AnimationTime> animationTime) {
        this.animationTime = animationTime;
    }

    public void setBuddySize(BuddySize buddySize) {
        this.buddySize = buddySize;
    }

    public void setCamera(CameraSetup camera) {
        this.camera = camera;
    }

    public void setCinematicMoves(List<CinematicMove> cinematicMoves) {
        this.cinematicMoves = cinematicMoves;
    }

    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setForms(List<PokemonForm> forms) {
        this.forms = forms;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public void setKmBuddyDistance(Integer kmBuddyDistance) {
        this.kmBuddyDistance = kmBuddyDistance;
    }

    public void setModelHeight(BigDecimal modelHeight) {
        this.modelHeight = modelHeight;
    }

    public void setModelScale(BigDecimal modelScale) {
        this.modelScale = modelScale;
    }

    public void setQuickMoves(List<QuickMove> quickMoves) {
        this.quickMoves = quickMoves;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public void setStats(PokemonStatistic stats) {
        this.stats = stats;
    }

    public void setTypes(List<BaseType> types) {
        this.types = types;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
