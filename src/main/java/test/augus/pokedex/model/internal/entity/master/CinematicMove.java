package test.augus.pokedex.model.internal.entity.master;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import test.augus.pokedex.constant.PokemonConstant;

@Entity
@DiscriminatorValue(PokemonConstant.MOVEMENT_CINEMATIC)
public class CinematicMove extends Move {
    public CinematicMove() {
    }

    public CinematicMove(String id, String name) {
        super(id, name);
    }
}
