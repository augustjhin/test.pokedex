package test.augus.pokedex.model.internal.jpa;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import test.augus.pokedex.model.internal.entity.Pokemon;

public interface PokemonJpaRepository extends JpaRepository<Pokemon, String>, JpaSpecificationExecutor<Pokemon> {
    @Query("select p from Pokemon p join p.types t where :type in (t.id)")
    List<Pokemon> findByType(@Param("type") String type, Sort sort);
}