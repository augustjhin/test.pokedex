package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "CAMERA_SETUP")
public class CameraSetup implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    @Column(precision = 10, scale = 1)
    private BigDecimal cylinderRadius;

    @Column(precision = 10, scale = 5)
    private BigDecimal diskRadius;

    @Column(precision = 10, scale = 1)
    private BigDecimal shoulderModeScale;

    public CameraSetup() {
    }

    public CameraSetup(String id, BigDecimal cylinderRadius, BigDecimal diskRadius, BigDecimal shoulderModeScale) {
        this.id = id;
        this.cylinderRadius = cylinderRadius;
        this.diskRadius = diskRadius;
        this.shoulderModeScale = shoulderModeScale;
    }

    public BigDecimal getCylinderRadius() {
        return cylinderRadius;
    }

    public BigDecimal getDiskRadius() {
        return diskRadius;
    }

    @Override
    public String getId() {
        return id;
    }

    public BigDecimal getShoulderModeScale() {
        return shoulderModeScale;
    }

    public void setCylinderRadius(BigDecimal cylinderRadius) {
        this.cylinderRadius = cylinderRadius;
    }

    public void setDiskRadius(BigDecimal diskRadius) {
        this.diskRadius = diskRadius;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setShoulderModeScale(BigDecimal shoulderModeScale) {
        this.shoulderModeScale = shoulderModeScale;
    }
}
