package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CURRENT_STATISTIC")
public class CurrentPokemonStatistic implements StatisticEntity {

    @Id
    protected String id;
    protected int baseAttack;
    protected int baseDefense;
    protected int baseStamina;

    public CurrentPokemonStatistic() {
        super();
    }

    public CurrentPokemonStatistic(String pokemonId, int baseAttack, int baseDefense, int baseStamina) {
        this.id = pokemonId;
        this.baseAttack = baseAttack;
        this.baseDefense = baseDefense;
        this.baseStamina = baseStamina;
    }

    @Override
    public int getBaseAttack() {
        return baseAttack;
    }

    @Override
    public int getBaseDefense() {
        return baseDefense;
    }

    @Override
    public int getBaseStamina() {
        return baseStamina;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setBaseAttack(int baseAttack) {
        this.baseAttack = baseAttack;
    }

    @Override
    public void setBaseDefense(int baseDefense) {
        this.baseDefense = baseDefense;
    }

    @Override
    public void setBaseStamina(int baseStamina) {
        this.baseStamina = baseStamina;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

}