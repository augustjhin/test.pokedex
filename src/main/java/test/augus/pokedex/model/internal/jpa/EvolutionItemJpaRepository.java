package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.master.EvolutionItem;

public interface EvolutionItemJpaRepository extends JpaRepository<EvolutionItem, String> {

}
