package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "POKEMON_ANIMATION_TIME")
public class AnimationTime implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;

    private String pokemonId;
    private int sequence;

    @Column(precision = 10, scale = 5)
    private BigDecimal time;

    public AnimationTime() {
    }

    public AnimationTime(String pokemonId, int sequence, BigDecimal time) {
        this.pokemonId = pokemonId;
        this.setSequence(sequence);
        this.time = time;
        this.id = pokemonId + "|" + sequence;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getPokemonId() {
        return pokemonId;
    }

    public int getSequence() {
        return sequence;
    }

    public BigDecimal getTime() {
        return time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPokemonId(String pokemon_id) {
        this.pokemonId = pokemon_id;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public void setTime(BigDecimal time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "AnimationTime [time=" + getTime() + ", id=" + id + "]";
    }
}
