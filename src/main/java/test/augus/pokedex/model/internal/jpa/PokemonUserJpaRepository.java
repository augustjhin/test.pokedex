package test.augus.pokedex.model.internal.jpa;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import test.augus.pokedex.model.internal.entity.PokemonUser;
import test.augus.pokedex.model.internal.entity.User;

public interface PokemonUserJpaRepository extends JpaRepository<PokemonUser, String>, JpaSpecificationExecutor<PokemonUser> {

    @Query("select pu from PokemonUser pu where pu.user = :user and pu.id = :id ")
    public PokemonUser find(@Param("user") User user, @Param("id") String id);

    @Query("select pu from PokemonUser pu where pu.user = :user")
    public List<PokemonUser> findAll(@Param("user") User user, Sort sort);
}
