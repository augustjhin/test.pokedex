package test.augus.pokedex.model.internal.entity;

public interface IBaseEntity {
    public String getId();
}
