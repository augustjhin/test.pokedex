package test.augus.pokedex.model.internal.entity.pokemon.attributes;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Entity
@Table(name = "POKEMON_ENCOUNTER_STATS")
public class Encounter implements IBaseEntity {
    @Id
    @Column(name = "ID")
    private String id;
    private BigDecimal attackProbability;

    private int attackTimer;
    private BigDecimal baseFleeRate;
    private BigDecimal baseCaptureRate;
    private BigDecimal cameraDistance;
    @Column(scale = 4)
    private BigDecimal collisionRadius;

    private int dodgeDistance;
    private BigDecimal dodgeProbability;
    private BigDecimal jumpTime;
    private BigDecimal maxPokemonActionFrequency;
    private BigDecimal minPokemonActionFrequency;
    @OneToOne
    private EncounterMovementType movementType;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private EncounterProbabilityByGender gender;

    public BigDecimal getAttackProbability() {
        return attackProbability;
    }

    public int getAttackTimer() {
        return attackTimer;
    }

    public BigDecimal getBaseCaptureRate() {
        return baseCaptureRate;
    }

    public BigDecimal getBaseFleeRate() {
        return baseFleeRate;
    }

    public BigDecimal getCameraDistance() {
        return cameraDistance;
    }

    public BigDecimal getCollisionRadius() {
        return collisionRadius;
    }

    public int getDodgeDistance() {
        return dodgeDistance;
    }

    public BigDecimal getDodgeProbability() {
        return dodgeProbability;
    }

    public EncounterProbabilityByGender getGender() {
        return gender;
    }

    @Override
    public String getId() {
        return null;
    }

    public BigDecimal getJumpTime() {
        return jumpTime;
    }

    public BigDecimal getMaxPokemonActionFrequency() {
        return maxPokemonActionFrequency;
    }

    public BigDecimal getMinPokemonActionFrequency() {
        return minPokemonActionFrequency;
    }

    public EncounterMovementType getMovementType() {
        return movementType;
    }

    public void setAttackProbability(BigDecimal attackProbability) {
        this.attackProbability = attackProbability;
    }

    public void setAttackTimer(int attackTimer) {
        this.attackTimer = attackTimer;
    }

    public void setBaseCaptureRate(BigDecimal baseCaptureRate) {
        this.baseCaptureRate = baseCaptureRate;
    }

    public void setBaseFleeRate(BigDecimal baseFleeRate) {
        this.baseFleeRate = baseFleeRate;
    }

    public void setCameraDistance(BigDecimal cameraDistance) {
        this.cameraDistance = cameraDistance;
    }

    public void setCollisionRadius(BigDecimal collisionRadius) {
        this.collisionRadius = collisionRadius;
    }

    public void setDodgeDistance(int dodgeDistance) {
        this.dodgeDistance = dodgeDistance;
    }

    public void setDodgeProbability(BigDecimal dodgeProbability) {
        this.dodgeProbability = dodgeProbability;
    }

    public void setGender(EncounterProbabilityByGender gender) {
        this.gender = gender;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setJumpTime(BigDecimal jumpTime) {
        this.jumpTime = jumpTime;
    }

    public void setMaxPokemonActionFrequency(BigDecimal maxPokemonActionFrequency) {
        this.maxPokemonActionFrequency = maxPokemonActionFrequency;
    }

    public void setMinPokemonActionFrequency(BigDecimal minPokemonActionFrequency) {
        this.minPokemonActionFrequency = minPokemonActionFrequency;
    }

    public void setMovementType(EncounterMovementType movementType) {
        this.movementType = movementType;
    }

}
