package test.augus.pokedex.model.internal.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import test.augus.pokedex.model.internal.entity.master.BaseType;

public interface BaseTypeJpaRepository extends JpaRepository<BaseType, String> {

}
