package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.CinematicMoveDTO;
import test.augus.pokedex.model.internal.entity.master.CinematicMove;
import test.augus.pokedex.model.internal.jpa.CinematicMoveJpaRepository;

@Repository
public class CinematicMovementDAO extends AbstractDAO<CinematicMove, CinematicMoveDTO> {
    @Autowired
    private CinematicMoveJpaRepository jpaRepository;

    @Override
    protected JpaRepository<CinematicMove, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public CinematicMoveDTO newInstance(CinematicMove entity) {
        return new CinematicMoveDTO(entity);
    }
}
