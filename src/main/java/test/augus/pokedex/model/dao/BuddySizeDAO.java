package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.BuddySizeDTO;
import test.augus.pokedex.model.internal.entity.master.BuddySize;
import test.augus.pokedex.model.internal.jpa.BuddySizeJpaRepository;

@Repository
public class BuddySizeDAO extends AbstractDAO<BuddySize, BuddySizeDTO> {
    @Autowired
    private BuddySizeJpaRepository jpaRepository;

    @Override
    protected JpaRepository<BuddySize, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public BuddySizeDTO newInstance(BuddySize entity) {
        return new BuddySizeDTO(entity);
    }
}
