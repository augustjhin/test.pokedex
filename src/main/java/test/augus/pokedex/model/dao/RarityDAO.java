package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.RarityDTO;
import test.augus.pokedex.model.internal.entity.master.Rarity;
import test.augus.pokedex.model.internal.jpa.RarityJpaRepository;

@Repository
public class RarityDAO extends AbstractDAO<Rarity, RarityDTO> {
    @Autowired
    private RarityJpaRepository jpaRepository;

    @Override
    protected JpaRepository<Rarity, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public RarityDTO newInstance(Rarity entity) {
        return new RarityDTO(entity);
    }
}
