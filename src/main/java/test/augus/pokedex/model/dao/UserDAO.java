package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.UserDTO;
import test.augus.pokedex.model.internal.entity.User;
import test.augus.pokedex.model.internal.jpa.UserJpaRepository;

@Repository
public class UserDAO extends AbstractDAO<User, UserDTO> {
    @Autowired
    private UserJpaRepository jpaRepository;

    @Override
    protected JpaRepository<User, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public UserDTO newInstance(User entity) {
        return new UserDTO(entity);
    }
}
