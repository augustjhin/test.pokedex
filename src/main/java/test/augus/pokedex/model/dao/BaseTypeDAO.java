package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.BaseTypeDTO;
import test.augus.pokedex.model.internal.entity.master.BaseType;
import test.augus.pokedex.model.internal.jpa.BaseTypeJpaRepository;

@Repository
public class BaseTypeDAO extends AbstractDAO<BaseType, BaseTypeDTO> {
    @Autowired
    private BaseTypeJpaRepository jpaRepository;

    @Override
    protected JpaRepository<BaseType, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public BaseTypeDTO newInstance(BaseType entity) {
        return new BaseTypeDTO(entity);
    }
}
