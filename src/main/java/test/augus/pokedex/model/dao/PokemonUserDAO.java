package test.augus.pokedex.model.dao;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import test.augus.pokedex.dto.PokemonUserDTO;
import test.augus.pokedex.dto.UserDTO;
import test.augus.pokedex.model.internal.entity.PokemonUser;
import test.augus.pokedex.model.internal.jpa.PokemonUserJpaRepository;

@Repository
public class PokemonUserDAO extends AbstractDAO<PokemonUser, PokemonUserDTO> {
    public static final String ERROR_USERNAME_REQUIRED = "User ID required";
    @Autowired
    private PokemonUserJpaRepository jpaRepository;

    public void delete(UserDTO user, String pokemonUserId) {
        Assert.notNull(user, ERROR_USERNAME_REQUIRED);
        Assert.notNull(user.getId(), ERROR_USERNAME_REQUIRED);
        Assert.notNull(pokemonUserId, ERROR_ENTITY_ID_IS_NULL);
        PokemonUser entity = jpaRepository.find(user.toEntity(), pokemonUserId);
        if (entity != null) {
            jpaRepository.delete(entity);
        }
    }

    public PokemonUserDTO get(UserDTO user, String pokemonUserId) {
        Assert.notNull(user, ERROR_USERNAME_REQUIRED);
        Assert.notNull(user.getId(), ERROR_USERNAME_REQUIRED);
        Assert.notNull(pokemonUserId, ERROR_ENTITY_ID_IS_NULL);
        PokemonUser entity = jpaRepository.find(user.toEntity(), pokemonUserId);
        return entity == null ? null : newInstance(entity);
    }

    public List<PokemonUserDTO> getAll(UserDTO user) {
        Assert.notNull(user, ERROR_USERNAME_REQUIRED);
        Assert.notNull(user.getId(), ERROR_USERNAME_REQUIRED);
        List<PokemonUser> entities = jpaRepository.findAll(user.toEntity(), getDefaultSort());
        return toDTO(entities);
    }

    @Override
    protected Sort getDefaultSort() {
        List<Order> orders = Arrays.asList(//
                new Order(Direction.DESC, "favourited"), //
                new Order(Direction.ASC, "pokemon.dex"), //
                new Order(Direction.DESC, "currentCP"));
        return Sort.by(orders);
    }

    @Override
    protected JpaRepository<PokemonUser, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public PokemonUserDTO newInstance(PokemonUser entity) {
        return new PokemonUserDTO(entity);
    }
}
