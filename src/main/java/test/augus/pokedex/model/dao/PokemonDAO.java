package test.augus.pokedex.model.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.Join;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.dto.PokemonParamDTO;
import test.augus.pokedex.dto.PokemonParamDTO.SortDTO;
import test.augus.pokedex.dto.pokemon.attributes.PokemonStatisticDTO;
import test.augus.pokedex.model.internal.entity.Pokemon;
import test.augus.pokedex.model.internal.entity.master.BaseType;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.PokemonStatistic;
import test.augus.pokedex.model.internal.jpa.PokemonJpaRepository;

@Repository
public class PokemonDAO extends AbstractDAO<Pokemon, PokemonDTO> {
    @Autowired
    private PokemonJpaRepository jpaRepository;

    private Specification<Pokemon> baseTypeEquals(String type) {
        return (root, query, builder) -> {
            Join<Pokemon, BaseType> baseType = root.join("types");
            return builder.equal(baseType.get("id"), type);
        };
    }

    public List<PokemonDTO> findByType(String type) {
        List<Pokemon> entities = jpaRepository.findByType(type, getDefaultSort());
        return toDTO(entities);
    }

    public List<PokemonDTO> getAll(@NonNull PokemonParamDTO params) {
        Sort sort = getSorting(params.getSorts());
        boolean hasTypeFilter = !StringUtils.isEmpty(params.getType());
        boolean hasStatFilter = params.getStatsFilter() != null;
        boolean hasFilter = hasTypeFilter || hasStatFilter;
        List<Pokemon> entities;
        if (hasFilter) {
            Specification<Pokemon> specification = Specification.where(null);
            if (hasTypeFilter) {
                specification = specification.and(baseTypeEquals(params.getType()));
            }
            if (hasStatFilter) {
                specification = specification.and(statsGreaterEquals(params.getStatsFilter()));
            }
            entities = jpaRepository.findAll(specification, sort);
        } else {
            entities = getJpaRepository().findAll(sort);
        }
        return toDTO(entities);
    }

    @Override
    protected JpaRepository<Pokemon, String> getJpaRepository() {
        return jpaRepository;
    }

    private Sort getSorting(SortDTO[] sorts) {
        Sort sort = getDefaultSort();
        if (sorts != null && sorts.length > 0) {
            List<Order> orders = new ArrayList<>(sorts.length);
            Set<String> fields = new HashSet<>(sorts.length);
            for (SortDTO param : sorts) {
                if (StringUtils.isEmpty(param.getField())) {
                    continue;
                }
                if (fields.add(param.getField())) {
                    Direction direction = Boolean.FALSE.equals(param.getAscending()) ? Sort.Direction.DESC : Sort.Direction.ASC;
                    orders.add(new Order(direction, param.getField()));
                }
            }
            sort = Sort.by(orders);
        }
        return sort;
    }

    @Override
    public PokemonDTO newInstance(Pokemon entity) {
        return new PokemonDTO(entity);
    }

    private Specification<Pokemon> statsGreaterEquals(PokemonStatisticDTO stats) {
        return (root, query, builder) -> {
            Join<Pokemon, PokemonStatistic> baseType = root.join("stats");
            return builder.and( //
                    builder.ge(baseType.get("baseAttack"), stats.getBaseAttack()), //
                    builder.ge(baseType.get("baseDefense"), stats.getBaseDefense()), //
                    builder.ge(baseType.get("baseStamina"), stats.getBaseStamina()));
        };
    }
}
