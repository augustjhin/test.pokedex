package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.BasePokemonDTO;
import test.augus.pokedex.model.internal.entity.BasePokemon;
import test.augus.pokedex.model.internal.jpa.BasePokemonJpaRepository;

@Repository
public class BasePokemonDAO extends AbstractDAO<BasePokemon, BasePokemonDTO> {
    @Autowired
    private BasePokemonJpaRepository jpaRepository;

    @Override
    protected JpaRepository<BasePokemon, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public BasePokemonDTO newInstance(BasePokemon entity) {
        return new BasePokemonDTO(entity);
    }
}
