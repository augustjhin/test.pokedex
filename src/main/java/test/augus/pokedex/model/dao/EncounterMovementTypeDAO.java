package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.pokemon.attributes.EncounterMovementTypeDTO;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.EncounterMovementType;
import test.augus.pokedex.model.internal.jpa.EncounterMovementTypeJpaRepository;

@Repository
public class EncounterMovementTypeDAO extends AbstractDAO<EncounterMovementType, EncounterMovementTypeDTO> {
    @Autowired
    private EncounterMovementTypeJpaRepository jpaRepository;

    @Override
    protected JpaRepository<EncounterMovementType, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public EncounterMovementTypeDTO newInstance(EncounterMovementType entity) {
        return new EncounterMovementTypeDTO(entity);
    }
}
