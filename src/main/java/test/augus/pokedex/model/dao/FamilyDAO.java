package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.FamilyDTO;
import test.augus.pokedex.model.internal.entity.master.Family;
import test.augus.pokedex.model.internal.jpa.FamilyJpaRepository;

@Repository
public class FamilyDAO extends AbstractDAO<Family, FamilyDTO> {
    @Autowired
    private FamilyJpaRepository jpaRepository;

    @Override
    protected JpaRepository<Family, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public FamilyDTO newInstance(Family entity) {
        return new FamilyDTO(entity);
    }
}
