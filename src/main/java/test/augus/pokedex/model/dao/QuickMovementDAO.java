package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.QuickMoveDTO;
import test.augus.pokedex.model.internal.entity.master.QuickMove;
import test.augus.pokedex.model.internal.jpa.QuickMoveJpaRepository;

@Repository
public class QuickMovementDAO extends AbstractDAO<QuickMove, QuickMoveDTO> {
    @Autowired
    private QuickMoveJpaRepository jpaRepository;

    @Override
    protected JpaRepository<QuickMove, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public QuickMoveDTO newInstance(QuickMove entity) {
        return new QuickMoveDTO(entity);
    }
}
