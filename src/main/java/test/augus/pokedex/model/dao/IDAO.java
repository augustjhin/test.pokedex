package test.augus.pokedex.model.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.lang.NonNull;

/**
 * Base supported DAO event.
 *
 * @author augustoportjhin
 *
 * @param <T>
 *            the entity type.
 */
public interface IDAO<T> {
    public static final String ERROR_ENTITY_IS_NULL = "Entity is null";
    public static final String ERROR_ENTITY_ID_IS_NULL = "Entity id must not null";
    public static final String ERROR_ENTITY_ID_IS_NOT_NULL = "Entity id must be null";

    /**
     * Delete existing entity by ID.
     * 
     * @param id
     *            Id of the entity
     */
    void delete(String id);

    /**
     * Delete existing entity by ID.
     *
     * @param obj
     *            the entity to be updated.
     */
    public void delete(@NonNull T obj);

    /**
     * Delete all entities.
     */
    public void deleteAll();

    /**
     * Get object by ID.
     *
     * @param id
     *            the ID of the entity.
     * @return the entity if exists, null if not found.
     */
    public T get(@NonNull String id);

    /**
     * Get all entities.
     *
     * @return all entity if exists, empty list if not found.
     */
    public List<T> getAll();

    /**
     * Insert new entity to repository.
     *
     * @param newObj
     *            the entity to be inserted
     * @return the inserted entity
     */
    public T insert(@NonNull T newObj);

    /**
     * Save all the entities.
     *
     * @param list
     *            the entities
     */
    public void insertAll(Collection<T> list);

    /**
     * Update existing entity by ID or create new entry if ID is not provided.
     *
     * @param obj
     *            the entity to be updated.
     * @return the updated entity.
     */
    public T update(@NonNull T obj);
}
