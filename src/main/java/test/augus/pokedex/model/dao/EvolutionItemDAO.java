package test.augus.pokedex.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.augus.pokedex.dto.master.EvolutionItemDTO;
import test.augus.pokedex.model.internal.entity.master.EvolutionItem;
import test.augus.pokedex.model.internal.jpa.EvolutionItemJpaRepository;

@Repository
public class EvolutionItemDAO extends AbstractDAO<EvolutionItem, EvolutionItemDTO> {
    @Autowired
    private EvolutionItemJpaRepository jpaRepository;

    @Override
    protected JpaRepository<EvolutionItem, String> getJpaRepository() {
        return jpaRepository;
    }

    @Override
    public EvolutionItemDTO newInstance(EvolutionItem entity) {
        return new EvolutionItemDTO(entity);
    }
}
