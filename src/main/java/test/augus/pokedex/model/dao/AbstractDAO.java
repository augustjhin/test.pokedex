package test.augus.pokedex.model.dao;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import test.augus.pokedex.dto.IBaseDTO;
import test.augus.pokedex.model.internal.entity.IBaseEntity;

@Repository
public abstract class AbstractDAO<E extends IBaseEntity, T extends IBaseDTO<E>> implements IDAO<T> {

    private static final Sort DEFAULT_SORT = Sort.by(Sort.Direction.ASC, "id");

    @Override
    public void delete(String id) {
        Assert.notNull(id, ERROR_ENTITY_ID_IS_NULL);
        getJpaRepository().deleteById(id);
    }

    @Override
    public void delete(T obj) {
        Assert.notNull(obj, ERROR_ENTITY_IS_NULL);
        delete(obj.getId());

    }

    @Override
    public void deleteAll() {
        getJpaRepository().deleteAll();
    }

    @Override
    public T get(String id) {
        Assert.notNull(id, ERROR_ENTITY_ID_IS_NULL);

        E entity = getJpaRepository().findById(id).orElse(null);
        return entity == null ? null : newInstance(entity);
    }

    @Override
    public List<T> getAll() {
        List<E> entities = getJpaRepository().findAll(getDefaultSort());
        return toDTO(entities);
    }

    /**
     * Default sort applied when running findAll/findAll with Filter
     *
     * @return the {@link Sort} object
     */
    protected Sort getDefaultSort() {
        return DEFAULT_SORT;
    }

    /**
     * Get Actual JpaRepository that responsible for JPA invocation.
     *
     * @return JpaRepository for entity &lt;E&gt;
     */
    protected abstract JpaRepository<E, String> getJpaRepository();

    @Override
    public T insert(T newObj) {
        Assert.notNull(newObj, ERROR_ENTITY_IS_NULL);

        E newEntity = newObj.toEntity();
        E savedEntity = getJpaRepository().save(newEntity);
        return newInstance(savedEntity);
    }

    @Override
    public void insertAll(Collection<T> list) {
        Assert.notNull(list, ERROR_ENTITY_IS_NULL);
        List<E> entities = list.stream().map(T::toEntity).collect(Collectors.toList());
        getJpaRepository().saveAll(entities);
    }

    /**
     * Return new DTO based on the entity.
     *
     * @param entity
     *            the entity to transform.
     * @return new DTO.
     */
    public abstract T newInstance(E entity);

    /**
     * Convert list of Entity to DTO
     *
     * @param entities
     *            the entities
     * @return DTO representative of the entity
     */
    protected List<T> toDTO(List<E> entities) {
        return entities.stream().map(this::newInstance).collect(Collectors.toList());
    }

    @Override
    public T update(T obj) {
        Assert.notNull(obj, ERROR_ENTITY_IS_NULL);

        E newEntity = obj.toEntity();
        E updatedEntity = getJpaRepository().save(newEntity);
        return newInstance(updatedEntity);
    }
}
