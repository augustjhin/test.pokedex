package test.augus.pokedex.constant;

public final class PokemonConstant {
    public static final String MOVEMENT_CINEMATIC = "cinematic";
    public static final String MOVEMENT_QUICK = "quick";
    public static final String TEST_USER = "user1@test.com";

    private PokemonConstant() {

    }
}
