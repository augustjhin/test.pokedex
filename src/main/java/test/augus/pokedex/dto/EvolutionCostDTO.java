package test.augus.pokedex.dto;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.dto.master.EvolutionItemDTO;
import test.augus.pokedex.model.internal.entity.EvolutionCost;

public class EvolutionCostDTO implements Serializable {
    private static final long serialVersionUID = -4763246167477435169L;
    private Integer candyCost;
    private Integer kmBuddyDistance;
    private EvolutionItemDTO evolutionItem;

    public EvolutionCostDTO(@NonNull EvolutionCost entity) {
        this.candyCost = entity.getCandyCost();
        this.kmBuddyDistance = entity.getKmBuddyDistance();
        if (entity.getEvolutionItem() != null) {
            evolutionItem = new EvolutionItemDTO(entity.getEvolutionItem());
        }
    }

    public Integer getCandyCost() {
        return candyCost;
    }

    public EvolutionItemDTO getEvolutionItem() {
        return evolutionItem;
    }

    public Integer getKmBuddyDistance() {
        return kmBuddyDistance;
    }

    public void setCandyCost(Integer candyCost) {
        this.candyCost = candyCost;
    }

    public void setEvolutionItem(EvolutionItemDTO evolutionItem) {
        this.evolutionItem = evolutionItem;
    }

    public void setKmBuddyDistance(Integer kmBuddyDistance) {
        this.kmBuddyDistance = kmBuddyDistance;
    }

    public EvolutionCost toEntity() {
        EvolutionCost entity = new EvolutionCost();
        entity.setCandyCost(candyCost);
        entity.setKmBuddyDistance(kmBuddyDistance);
        if (evolutionItem != null) {
            entity.setEvolutionItem(evolutionItem.toEntity());
        }
        return entity;
    }
}
