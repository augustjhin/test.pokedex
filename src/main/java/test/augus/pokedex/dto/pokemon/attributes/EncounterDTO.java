package test.augus.pokedex.dto.pokemon.attributes;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.pokemon.attributes.Encounter;

public class EncounterDTO implements Serializable {
    private static final long serialVersionUID = -3705749840114534145L;
    private String id;
    private BigDecimal attackProbability;
    private int attackTimer;
    private BigDecimal baseFleeRate;
    private BigDecimal baseCaptureRate;
    private BigDecimal cameraDistance;
    private BigDecimal collisionRadius;
    private int dodgeDistance;
    private BigDecimal dodgeProbability;
    private BigDecimal jumpTime;
    private BigDecimal maxPokemonActionFrequency;
    private BigDecimal minPokemonActionFrequency;
    private EncounterMovementTypeDTO movementType;
    private EncounterProbabilityByGenderDTO gender;

    public EncounterDTO() {
    }

    public EncounterDTO(@NonNull Encounter encounter) {
        this.id = encounter.getId();
        attackProbability = encounter.getAttackProbability();
        attackTimer = encounter.getAttackTimer();
        baseFleeRate = encounter.getBaseFleeRate();
        baseCaptureRate = encounter.getBaseCaptureRate();
        cameraDistance = encounter.getCameraDistance();
        collisionRadius = encounter.getCollisionRadius();
        dodgeDistance = encounter.getDodgeDistance();
        dodgeProbability = encounter.getDodgeProbability();
        jumpTime = encounter.getJumpTime();
        maxPokemonActionFrequency = encounter.getMaxPokemonActionFrequency();
        minPokemonActionFrequency = encounter.getMinPokemonActionFrequency();
        if (encounter.getMovementType() != null) {
            movementType = new EncounterMovementTypeDTO(encounter.getMovementType());
        }
        if (encounter.getGender() != null) {
            gender = new EncounterProbabilityByGenderDTO(encounter.getGender());
        }
    }

    public BigDecimal getAttackProbability() {
        return attackProbability;
    }

    public int getAttackTimer() {
        return attackTimer;
    }

    public BigDecimal getBaseCaptureRate() {
        return baseCaptureRate;
    }

    public BigDecimal getBaseFleeRate() {
        return baseFleeRate;
    }

    public BigDecimal getCameraDistance() {
        return cameraDistance;
    }

    public BigDecimal getCollisionRadius() {
        return collisionRadius;
    }

    public int getDodgeDistance() {
        return dodgeDistance;
    }

    public BigDecimal getDodgeProbability() {
        return dodgeProbability;
    }

    public EncounterProbabilityByGenderDTO getGender() {
        return gender;
    }

    public BigDecimal getJumpTime() {
        return jumpTime;
    }

    public BigDecimal getMaxPokemonActionFrequency() {
        return maxPokemonActionFrequency;
    }

    public BigDecimal getMinPokemonActionFrequency() {
        return minPokemonActionFrequency;
    }

    public EncounterMovementTypeDTO getMovementType() {
        return movementType;
    }

    public void setAttackProbability(BigDecimal attackProbability) {
        this.attackProbability = attackProbability;
    }

    public void setAttackTimer(int attackTimer) {
        this.attackTimer = attackTimer;
    }

    public void setBaseCaptureRate(BigDecimal baseCaptureRate) {
        this.baseCaptureRate = baseCaptureRate;
    }

    public void setBaseFleeRate(BigDecimal baseFleeRate) {
        this.baseFleeRate = baseFleeRate;
    }

    public void setCameraDistance(BigDecimal cameraDistance) {
        this.cameraDistance = cameraDistance;
    }

    public void setCollisionRadius(BigDecimal collisionRadius) {
        this.collisionRadius = collisionRadius;
    }

    public void setDodgeDistance(int dodgeDistance) {
        this.dodgeDistance = dodgeDistance;
    }

    public void setDodgeProbability(BigDecimal dodgeProbability) {
        this.dodgeProbability = dodgeProbability;
    }

    public void setGender(EncounterProbabilityByGenderDTO gender) {
        this.gender = gender;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setJumpTime(BigDecimal jumpTime) {
        this.jumpTime = jumpTime;
    }

    public void setMaxPokemonActionFrequency(BigDecimal maxPokemonActionFrequency) {
        this.maxPokemonActionFrequency = maxPokemonActionFrequency;
    }

    public void setMinPokemonActionFrequency(BigDecimal minPokemonActionFrequency) {
        this.minPokemonActionFrequency = minPokemonActionFrequency;
    }

    public void setMovementType(EncounterMovementTypeDTO movementType) {
        this.movementType = movementType;
    }

    public Encounter toEntity() {
        Encounter encounter = new Encounter();
        encounter.setId(id);
        encounter.setAttackProbability(attackProbability);
        encounter.setAttackTimer(attackTimer);
        encounter.setBaseFleeRate(baseFleeRate);
        encounter.setBaseCaptureRate(baseCaptureRate);
        encounter.setCameraDistance(cameraDistance);
        encounter.setCollisionRadius(collisionRadius);
        encounter.setDodgeDistance(dodgeDistance);
        encounter.setDodgeProbability(dodgeProbability);
        encounter.setJumpTime(jumpTime);
        encounter.setMaxPokemonActionFrequency(maxPokemonActionFrequency);
        encounter.setMinPokemonActionFrequency(minPokemonActionFrequency);
        encounter.setMovementType(movementType == null ? null : movementType.toEntity());
        if (gender != null) {
            gender.setId(id);
            encounter.setGender(gender.toEntity());
        }
        return encounter;

    }
}
