package test.augus.pokedex.dto.pokemon.attributes;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.dto.IBaseDTO;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.PokemonForm;

public class PokemonFormDTO implements Serializable, IBaseDTO<PokemonForm> {
    private static final long serialVersionUID = 7437597355829676330L;
    private String pokemonId;
    private String id;
    private String name;

    public PokemonFormDTO() {
    }

    public PokemonFormDTO(@NonNull PokemonForm stats) {
        this.pokemonId = stats.getPokemonId();
        this.id = stats.getId();
        this.name = stats.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof PokemonFormDTO) {
            PokemonFormDTO anotherObj = (PokemonFormDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPokemonId() {
        return pokemonId;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPokemonId(String pokemonId) {
        this.pokemonId = pokemonId;
    }

    @Override
    public PokemonForm toEntity() {
        return new PokemonForm(getPokemonId(), getId(), getName());
    }
}
