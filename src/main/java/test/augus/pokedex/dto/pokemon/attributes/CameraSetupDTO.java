package test.augus.pokedex.dto.pokemon.attributes;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.pokemon.attributes.CameraSetup;

public class CameraSetupDTO implements Serializable {

    private static final long serialVersionUID = 4410685871969081231L;

    private String id;

    private BigDecimal cylinderRadius;

    private BigDecimal diskRadius;

    private BigDecimal shoulderModeScale;

    public CameraSetupDTO() {
    }

    public CameraSetupDTO(@NonNull CameraSetup camera) {
        this.id = camera.getId();
        this.cylinderRadius = camera.getCylinderRadius();
        this.diskRadius = camera.getDiskRadius();
        this.shoulderModeScale = camera.getShoulderModeScale();
    }

    public BigDecimal getCylinderRadius() {
        return cylinderRadius;
    }

    public BigDecimal getDiskRadius() {
        return diskRadius;
    }

    public BigDecimal getShoulderModeScale() {
        return shoulderModeScale;
    }

    public void setCylinderRadius(BigDecimal cylinderRadius) {
        this.cylinderRadius = cylinderRadius;
    }

    public void setDiskRadius(BigDecimal diskRadius) {
        this.diskRadius = diskRadius;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setShoulderModeScale(BigDecimal shoulderModeScale) {
        this.shoulderModeScale = shoulderModeScale;
    }

    public CameraSetup toEntity() {
        return new CameraSetup(id, cylinderRadius, diskRadius, shoulderModeScale);
    }
}
