package test.augus.pokedex.dto.pokemon.attributes;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.pokemon.attributes.EncounterProbabilityByGender;

public class EncounterProbabilityByGenderDTO implements Serializable {

    private static final long serialVersionUID = -7639843023317512053L;
    private String id;
    private BigDecimal malePercent;
    private BigDecimal femalePercent;

    public EncounterProbabilityByGenderDTO() {
    }

    public EncounterProbabilityByGenderDTO(@NonNull EncounterProbabilityByGender entity) {
        this.id = entity.getId();
        this.malePercent = entity.getMalePercent();
        this.femalePercent = entity.getFemalePercent();
    }

    public BigDecimal getFemalePercent() {
        return femalePercent;
    }

    public BigDecimal getMalePercent() {
        return malePercent;
    }

    public void setFemalePercent(BigDecimal femalePercent) {
        this.femalePercent = femalePercent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMalePercent(BigDecimal malePercent) {
        this.malePercent = malePercent;
    }

    public EncounterProbabilityByGender toEntity() {
        return new EncounterProbabilityByGender(id, malePercent, femalePercent);
    }
}
