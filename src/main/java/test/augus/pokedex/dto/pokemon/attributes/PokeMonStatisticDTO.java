package test.augus.pokedex.dto.pokemon.attributes;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.pokemon.attributes.PokemonStatistic;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.StatisticEntity;

public class PokemonStatisticDTO implements Serializable {

    private static final long serialVersionUID = 4410685871969081230L;
    private String id;
    private int baseAttack;
    private int baseDefense;

    private int baseStamina;

    public PokemonStatisticDTO() {
    }

    public PokemonStatisticDTO(@NonNull StatisticEntity stats) {
        this.id = stats.getId();
        this.baseAttack = stats.getBaseAttack();
        this.baseDefense = stats.getBaseDefense();
        this.baseStamina = stats.getBaseStamina();
    }

    public PokemonStatisticDTO(String id, int baseAttack, int baseDefense, int baseStamina) {
        this.id = id;
        this.baseAttack = baseAttack;
        this.baseDefense = baseDefense;
        this.baseStamina = baseStamina;
    }

    public int getBaseAttack() {
        return baseAttack;
    }

    public int getBaseDefense() {
        return baseDefense;
    }

    public int getBaseStamina() {
        return baseStamina;
    }

    public void setBaseAttack(int baseAttack) {
        this.baseAttack = baseAttack;
    }

    public void setBaseDefense(int baseDefense) {
        this.baseDefense = baseDefense;
    }

    public void setBaseStamina(int baseStamina) {
        this.baseStamina = baseStamina;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PokemonStatistic toEntity() {
        return new PokemonStatistic(id, baseAttack, baseDefense, baseStamina);
    }
}
