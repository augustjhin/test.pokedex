package test.augus.pokedex.dto.pokemon.attributes;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.dto.IBaseDTO;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.EncounterMovementType;

public class EncounterMovementTypeDTO implements Serializable, IBaseDTO<EncounterMovementType> {
    private static final long serialVersionUID = -249560124946925170L;
    private String id;
    private String name;

    public EncounterMovementTypeDTO() {
    }

    public EncounterMovementTypeDTO(@NonNull EncounterMovementType entity) {
        this.id = entity.getId();
        this.name = entity.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof EncounterMovementTypeDTO) {
            EncounterMovementTypeDTO anotherObj = (EncounterMovementTypeDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public EncounterMovementType toEntity() {
        return new EncounterMovementType(getId(), getName());
    }

    @Override
    public String toString() {
        return "EncounterMovementTypeDTO [name=" + name + ", id=" + id + "]";
    }
}
