package test.augus.pokedex.dto;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.BasePokemon;

public class BasePokemonDTO implements Serializable, IBaseDTO<BasePokemon> {
    private static final long serialVersionUID = 4187826419708914792L;
    private String id;
    private String name;
    private Integer dex;
    private Integer maxCP;

    public BasePokemonDTO() {

    }

    public BasePokemonDTO(@NonNull BasePokemon entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.dex = entity.getDex();
        this.maxCP = entity.getMaxCP();
    }

    public BasePokemonDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BasePokemonDTO) {
            BasePokemonDTO anotherObj = (BasePokemonDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    public Integer getDex() {
        return dex;
    }

    @Override
    public String getId() {
        return id;
    }

    public Integer getMaxCP() {
        return maxCP;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setDex(Integer dex) {
        this.dex = dex;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMaxCP(Integer maxCP) {
        this.maxCP = maxCP;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BasePokemon toEntity() {
        BasePokemon pokemon = new BasePokemon(getId(), getName());
        pokemon.setDex(dex);
        pokemon.setMaxCP(maxCP);
        return pokemon;
    }

    @Override
    public String toString() {
        return "BasePokemonDTO [name=" + name + ", id=" + id + "]";
    }
}
