package test.augus.pokedex.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import test.augus.pokedex.dto.pokemon.attributes.PokemonStatisticDTO;
import test.augus.pokedex.model.internal.entity.PokemonUser;
import test.augus.pokedex.model.internal.entity.User;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.CurrentPokemonStatistic;

/**
 * Entity recording type of pokemon owned by an user.
 *
 * @author augustoportjhin
 *
 */
public class PokemonUserDTO implements Serializable, IBaseDTO<PokemonUser> {
    private static final long serialVersionUID = -3798345069243392477L;
    private String id;
    private BasePokemonDTO pokemon;
    private String userId;
    private String customName;
    private Integer currentCP;
    private Date caughtDate;

    private PokemonStatisticDTO currentStats;
    private Boolean favourited;

    /**
     * Default constructor
     */
    public PokemonUserDTO() { // NOSONAR

    }

    public PokemonUserDTO(PokemonUser entity) {
        setId(entity.getId());
        pokemon = new BasePokemonDTO(entity.getPokemon());
        customName = entity.getCustomName();
        currentCP = entity.getCurrentCP();
        caughtDate = entity.getCaughtDate();
        favourited = entity.getFavourited();
        currentStats = new PokemonStatisticDTO(entity.getCurrentStats());
        userId = entity.getUser().getId();
    }

    public Date getCaughtDate() {
        return caughtDate;
    }

    public Integer getCurrentCP() {
        return currentCP;
    }

    public PokemonStatisticDTO getCurrentStats() {
        return currentStats;
    }

    public String getCustomName() {
        return customName;
    }

    public Boolean getFavourited() {
        return favourited;
    }

    @Override
    public String getId() {
        return id;
    }

    public BasePokemonDTO getPokemon() {
        return pokemon;
    }

    public void setCaughtDate(Date caughtDate) {
        this.caughtDate = caughtDate;
    }

    public void setCurrentCP(Integer currentCP) {
        this.currentCP = currentCP;
    }

    public void setCurrentStats(PokemonStatisticDTO stats) {
        this.currentStats = stats;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public void setFavourited(Boolean favourited) {
        this.favourited = favourited;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPokemon(BasePokemonDTO pokemon) {
        this.pokemon = pokemon;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public PokemonUser toEntity() {
        PokemonUser pokemonUser = new PokemonUser();
        pokemonUser.setId(getId() == null ? UUID.randomUUID().toString() : getId());
        pokemonUser.setCustomName(customName);
        pokemonUser.setCurrentCP(currentCP == null ? 0 : currentCP);
        if (caughtDate != null) {
            pokemonUser.setCaughtDate(new Timestamp(caughtDate.getTime()));
        }
        pokemonUser.setFavourited(favourited);
        pokemonUser.setCurrentStats(
                new CurrentPokemonStatistic(pokemonUser.getId(), currentStats.getBaseAttack(), currentStats.getBaseDefense(), currentStats.getBaseStamina()));
        pokemonUser.setPokemon(pokemon.toEntity());

        pokemonUser.setUser(new User(userId, userId));
        return pokemonUser;
    }

    @Override
    public String toString() {
        return "Pokemon owned [name=" + getPokemon().getName() + ", currenCP=" + currentCP + "]";
    }
}
