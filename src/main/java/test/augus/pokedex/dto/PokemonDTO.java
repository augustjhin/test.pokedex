package test.augus.pokedex.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import test.augus.pokedex.dto.master.BaseTypeDTO;
import test.augus.pokedex.dto.master.BuddySizeDTO;
import test.augus.pokedex.dto.master.CinematicMoveDTO;
import test.augus.pokedex.dto.master.FamilyDTO;
import test.augus.pokedex.dto.master.QuickMoveDTO;
import test.augus.pokedex.dto.master.RarityDTO;
import test.augus.pokedex.dto.pokemon.attributes.CameraSetupDTO;
import test.augus.pokedex.dto.pokemon.attributes.EncounterDTO;
import test.augus.pokedex.dto.pokemon.attributes.PokemonFormDTO;
import test.augus.pokedex.dto.pokemon.attributes.PokemonStatisticDTO;
import test.augus.pokedex.model.internal.entity.IBaseEntity;
import test.augus.pokedex.model.internal.entity.Pokemon;
import test.augus.pokedex.model.internal.entity.pokemon.attributes.AnimationTime;

/**
 * Main Pokemon entity.
 *
 * @author augustoportjhin
 *
 */
@JsonInclude(Include.NON_NULL)
public class PokemonDTO implements Serializable, Comparable<PokemonDTO>, IBaseDTO<Pokemon> {
    private static final long serialVersionUID = -7899729714237688570L;

    private String id;
    private String name;
    private Integer dex;
    private Integer maxCP;

    // measurement
    private BigDecimal height;
    private BigDecimal weight;
    private BigDecimal modelHeight;
    private BigDecimal modelScale;
    private Integer kmBuddyDistance;
    private BigDecimal[] animationTime;

    private BuddySizeDTO buddySize;

    private CinematicMoveDTO[] cinematicMoves;
    private QuickMoveDTO[] quickMoves;
    private FamilyDTO family;
    private PokemonStatisticDTO stats;
    private CameraSetupDTO camera;
    private BaseTypeDTO[] types;
    private PokemonFormDTO[] forms;
    private RarityDTO rarity;
    private EncounterDTO encounter;

    /**
     * Default constructor
     */
    public PokemonDTO() { // NOSONAR

    }

    public PokemonDTO(Pokemon pokemon) {
        id = pokemon.getId();
        name = pokemon.getName();
        dex = pokemon.getDex();
        maxCP = pokemon.getMaxCP();
        height = pokemon.getHeight();
        weight = pokemon.getWeight();
        modelHeight = pokemon.getModelHeight();
        modelScale = pokemon.getModelScale();
        kmBuddyDistance = pokemon.getKmBuddyDistance();
        buddySize = new BuddySizeDTO(pokemon.getBuddySize());
        animationTime = pokemon.getAnimationTime().stream() //
                .sorted((t1, t2) -> Integer.compare(t1.getSequence(), t2.getSequence())) //
                .map(AnimationTime::getTime).toArray(BigDecimal[]::new);
        cinematicMoves = pokemon.getCinematicMoves().stream().map(CinematicMoveDTO::new).toArray(CinematicMoveDTO[]::new);
        quickMoves = pokemon.getCinematicMoves().stream().map(QuickMoveDTO::new).toArray(QuickMoveDTO[]::new);
        family = new FamilyDTO(pokemon.getFamily());
        stats = new PokemonStatisticDTO(pokemon.getStats());
        camera = new CameraSetupDTO(pokemon.getCamera());
        types = pokemon.getTypes().stream().map(BaseTypeDTO::new).toArray(BaseTypeDTO[]::new);
        forms = pokemon.getForms().stream().map(PokemonFormDTO::new).toArray(PokemonFormDTO[]::new);
        rarity = pokemon.getRarity() == null ? null : new RarityDTO(pokemon.getRarity());
        encounter = new EncounterDTO(pokemon.getEncounter());
    }
    // evolution: Evolution;

    @Override
    public int compareTo(PokemonDTO o) {
        if (o == null) {
            return 1;
        }
        return Integer.compare(this.dex, o.dex);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof PokemonDTO) {
            PokemonDTO anotherObj = (PokemonDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    public BigDecimal[] getAnimationTime() {
        return animationTime;
    }

    private List<AnimationTime> getAnimationTimeAsList() {
        if (animationTime == null || animationTime.length == 0) {
            return Collections.emptyList();
        }

        List<AnimationTime> list = new ArrayList<>(animationTime.length);
        for (int i = 0; i < animationTime.length; i++) {
            list.add(new AnimationTime(id, i, animationTime[i]));
        }
        return list;
    }

    public BuddySizeDTO getBuddySize() {
        return buddySize;
    }

    public CameraSetupDTO getCamera() {
        return camera;
    }

    public CinematicMoveDTO[] getCinematicMoves() {
        return cinematicMoves;
    }

    /**
     * Shows the default sequence of object when displayed.
     */
    public Integer getDex() {
        return dex;
    }

    public EncounterDTO getEncounter() {
        return encounter;
    }

    public FamilyDTO getFamily() {
        return family;
    }

    public PokemonFormDTO[] getForms() {
        return forms;
    }

    public BigDecimal getHeight() {
        return height;
    }

    @Override
    public String getId() {
        return id;
    }

    public Integer getKmBuddyDistance() {
        return kmBuddyDistance;
    }

    public Integer getMaxCP() {
        return maxCP;
    }

    public BigDecimal getModelHeight() {
        return modelHeight;
    }

    public BigDecimal getModelScale() {
        return modelScale;
    }

    public String getName() {
        return name;
    }

    public QuickMoveDTO[] getQuickMoves() {
        return quickMoves;
    }

    public RarityDTO getRarity() {
        return rarity;
    }

    public PokemonStatisticDTO getStats() {
        return stats;
    }

    public BaseTypeDTO[] getTypes() {
        return types;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setAnimationTime(BigDecimal[] animationTime) {
        this.animationTime = animationTime;
    }

    public void setBuddySize(BuddySizeDTO buddySize) {
        this.buddySize = buddySize;
    }

    public void setCamera(CameraSetupDTO camera) {
        this.camera = camera;
    }

    public void setCinematicMoves(CinematicMoveDTO[] cinematicMoves) {
        this.cinematicMoves = cinematicMoves;
    }

    public void setDex(Integer dex) {
        this.dex = dex;
    }

    public void setEncounter(EncounterDTO encounter) {
        this.encounter = encounter;
    }

    public void setFamily(FamilyDTO family) {
        this.family = family;
    }

    public void setForms(PokemonFormDTO[] forms) {
        this.forms = forms;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setKmBuddyDistance(Integer kmBuddyDistance) {
        this.kmBuddyDistance = kmBuddyDistance;
    }

    public void setMaxCP(Integer maxCP) {
        this.maxCP = maxCP;
    }

    public void setModelHeight(BigDecimal modelHeight) {
        this.modelHeight = modelHeight;
    }

    public void setModelScale(BigDecimal modelScale) {
        this.modelScale = modelScale;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuickMoves(QuickMoveDTO[] quickMoves) {
        this.quickMoves = quickMoves;
    }

    public void setRarity(RarityDTO rarity) {
        this.rarity = rarity;
    }

    public void setStats(PokemonStatisticDTO stats) {
        this.stats = stats;
    }

    public void setTypes(BaseTypeDTO[] types) {
        this.types = types;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    @Override
    public Pokemon toEntity() {
        Pokemon pokemon = new Pokemon();
        pokemon.setId(getId());
        pokemon.setName(getId());
        pokemon.setDex(getDex());
        pokemon.setMaxCP(getMaxCP());
        pokemon.setHeight(getHeight());
        pokemon.setWeight(getWeight());
        pokemon.setModelHeight(getModelHeight());
        pokemon.setModelScale(getModelScale());
        pokemon.setKmBuddyDistance(getKmBuddyDistance());
        pokemon.setBuddySize(buddySize == null ? null : getBuddySize().toEntity());
        pokemon.setAnimationTime(getAnimationTimeAsList());
        pokemon.setCinematicMoves(toEntity(cinematicMoves));
        pokemon.setQuickMoves(toEntity(quickMoves));
        pokemon.setFamily(family == null ? null : getFamily().toEntity());
        pokemon.setRarity(rarity == null ? null : rarity.toEntity());
        if (stats != null) {
            stats.setId(id);
            pokemon.setStats(stats.toEntity());
        }
        if (getCamera() != null) {
            getCamera().setId(id);
            pokemon.setCamera(getCamera().toEntity());
        }
        pokemon.setTypes(toEntity(getTypes()));
        if (forms != null) {
            for (PokemonFormDTO form : forms) {
                form.setPokemonId(id);
            }
        }
        pokemon.setForms(toEntity(getForms()));
        if (encounter != null) {
            encounter.setId(id);
            pokemon.setEncounter(encounter.toEntity());
        }
        return pokemon;
    }

    private <E extends IBaseEntity, D extends IBaseDTO<E>> List<E> toEntity(D[] source) {
        if (source == null || source.length == 0) {
            return Collections.emptyList();
        }

        List<E> list = new ArrayList<>(source.length);
        for (D type : source) {
            list.add(type.toEntity());
        }
        return list;
    }

    @Override
    public String toString() {
        return "Pokemon [name=" + name + ", id=" + id + ", dex=" + dex + "]";
    }
}
