package test.augus.pokedex.dto;

public class BaseEvolutionBranchDTO {
    private BasePokemonDTO target;
    private EvolutionCostDTO costToEvolve;

    public EvolutionCostDTO getCostToEvolve() {
        return costToEvolve;
    }

    public BasePokemonDTO getTarget() {
        return target;
    }

    public void setCostToEvolve(EvolutionCostDTO costToEvolve) {
        this.costToEvolve = costToEvolve;
    }

    public void setTarget(BasePokemonDTO target) {
        this.target = target;
    }
}
