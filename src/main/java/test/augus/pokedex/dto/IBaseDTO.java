package test.augus.pokedex.dto;

import test.augus.pokedex.model.internal.entity.IBaseEntity;

public interface IBaseDTO<T extends IBaseEntity> {
    public String getId();

    public T toEntity();
}
