package test.augus.pokedex.dto;

import test.augus.pokedex.dto.pokemon.attributes.PokemonStatisticDTO;

/**
 * DTO describing supported parameters for retrieving pokedex.
 *
 * @author augustoportjhin
 *
 */
public class PokemonParamDTO {
    public static class SortDTO {
        private String field;
        private Boolean ascending;

        public SortDTO() {
        }

        /**
         * Construct new SortDTO with ascending defaulted to true.
         *
         * @param field
         *            fieldname to sort
         */
        public SortDTO(String field) {
            this.field = field;
            this.ascending = Boolean.TRUE;
        }

        public SortDTO(String field, Boolean ascending) {
            this.field = field;
            this.ascending = ascending;
        }

        public Boolean getAscending() {
            return ascending;
        }

        public String getField() {
            return field;
        }

        public void setAscending(Boolean ascending) {
            this.ascending = ascending;
        }

        public void setField(String field) {
            this.field = field;
        }
    }

    private SortDTO[] sorts;
    private PokemonStatisticDTO statsFilter;
    private String type;

    public SortDTO[] getSorts() {
        return sorts;
    }

    public PokemonStatisticDTO getStatsFilter() {
        return statsFilter;
    }

    public String getType() {
        return type;
    }

    public void setSorts(SortDTO... sorts) {
        this.sorts = sorts;
    }

    public void setStatsFilter(PokemonStatisticDTO statsFilter) {
        this.statsFilter = statsFilter;
    }

    public void setType(String type) {
        this.type = type;
    }

}
