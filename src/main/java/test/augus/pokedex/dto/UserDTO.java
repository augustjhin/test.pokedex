package test.augus.pokedex.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.User;

public class UserDTO implements Serializable, IBaseDTO<User> {
    private static final long serialVersionUID = 4187826419708914792L;
    private String id;
    private String name;
    private Date joinDate;

    public UserDTO() {

    }

    public UserDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserDTO(@NonNull User entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.setJoinDate(entity.getJoinDate());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof UserDTO) {
            UserDTO anotherObj = (UserDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public User toEntity() {
        User user = new User();
        user.setId(getId());
        user.setName(getName());
        if (joinDate != null) {
            user.setJoinDate(new Timestamp(joinDate.getTime()));
        }
        return user;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", id=" + id + "]";
    }
}
