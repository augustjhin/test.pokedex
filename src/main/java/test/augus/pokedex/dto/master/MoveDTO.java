package test.augus.pokedex.dto.master;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.dto.IBaseDTO;
import test.augus.pokedex.model.internal.entity.master.Move;

public abstract class MoveDTO<T extends Move> implements Serializable, IBaseDTO<T> {
    private static final long serialVersionUID = 6484935493955039845L;
    private String id;
    private String name;

    public MoveDTO() {

    }

    public MoveDTO(@NonNull Move move) {
        this.id = move.getId();
        this.name = move.getName();
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
