package test.augus.pokedex.dto.master;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.master.Move;
import test.augus.pokedex.model.internal.entity.master.QuickMove;

public class QuickMoveDTO extends MoveDTO<QuickMove> implements Serializable {

    private static final long serialVersionUID = -7027791174840740002L;

    public QuickMoveDTO() {
    }

    public QuickMoveDTO(@NonNull Move move) {
        super(move);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof QuickMoveDTO) {
            MoveDTO<?> anotherObj = (QuickMoveDTO) obj;
            return this.getId().equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public QuickMove toEntity() {
        return new QuickMove(getId(), getName());
    }
}
