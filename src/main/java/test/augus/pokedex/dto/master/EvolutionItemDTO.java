package test.augus.pokedex.dto.master;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.dto.IBaseDTO;
import test.augus.pokedex.model.internal.entity.master.EvolutionItem;

public class EvolutionItemDTO implements Serializable, IBaseDTO<EvolutionItem> {
    private static final long serialVersionUID = 2286013049020527899L;
    private String id;
    private String name;

    public EvolutionItemDTO() {

    }

    public EvolutionItemDTO(@NonNull EvolutionItem entity) {
        this.id = entity.getId();
        this.name = entity.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof EvolutionItemDTO) {
            EvolutionItemDTO anotherObj = (EvolutionItemDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public EvolutionItem toEntity() {
        return new EvolutionItem(getId(), getName());
    }

    @Override
    public String toString() {
        return "EvolutionItemDTO [name=" + name + ", id=" + id + "]";
    }
}
