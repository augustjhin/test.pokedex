package test.augus.pokedex.dto.master;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.dto.IBaseDTO;
import test.augus.pokedex.model.internal.entity.master.BaseType;

public class BaseTypeDTO implements Serializable, IBaseDTO<BaseType> {
    private static final long serialVersionUID = -249560124946925170L;
    private String id;
    private String name;

    public BaseTypeDTO() {

    }

    public BaseTypeDTO(@NonNull BaseType baseType) {
        this.id = baseType.getId();
        this.name = baseType.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BaseTypeDTO) {
            BaseTypeDTO anotherObj = (BaseTypeDTO) obj;
            return this.id.equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return -1;
        }
        return id.hashCode();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BaseType toEntity() {
        return new BaseType(getId(), getName());
    }

    @Override
    public String toString() {
        return "BaseTypeDTO [name=" + name + ", id=" + id + "]";
    }
}
