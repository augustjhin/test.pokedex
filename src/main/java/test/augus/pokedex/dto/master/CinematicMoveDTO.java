package test.augus.pokedex.dto.master;

import java.io.Serializable;

import org.springframework.lang.NonNull;

import test.augus.pokedex.model.internal.entity.master.CinematicMove;
import test.augus.pokedex.model.internal.entity.master.Move;

public class CinematicMoveDTO extends MoveDTO<CinematicMove> implements Serializable {

    private static final long serialVersionUID = -22587128753932388L;

    public CinematicMoveDTO() {
    }

    public CinematicMoveDTO(@NonNull Move move) {
        super(move);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CinematicMoveDTO) {
            MoveDTO<?> anotherObj = (CinematicMoveDTO) obj;
            return this.getId().equals(anotherObj.getId());
        }
        return false;
    }

    @Override
    public CinematicMove toEntity() {
        return new CinematicMove(getId(), getName());
    }
}
