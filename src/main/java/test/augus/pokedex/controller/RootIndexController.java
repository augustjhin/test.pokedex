package test.augus.pokedex.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This is REST controller that handle request to index page.
 * 
 * @author augustoportjhin
 *
 */
@Controller
public class RootIndexController {
    public static final String WELCOME_MESSAGE = "Welcome to Pokedex";

    @GetMapping("/")
    public @ResponseBody String greeting() {
        return WELCOME_MESSAGE;
    }

}