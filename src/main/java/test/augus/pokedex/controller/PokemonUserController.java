package test.augus.pokedex.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.augus.pokedex.constant.PokemonConstant;
import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.dto.PokemonUserDTO;
import test.augus.pokedex.dto.UserDTO;
import test.augus.pokedex.model.dao.PokemonDAO;
import test.augus.pokedex.model.dao.PokemonUserDAO;

/**
 * This is the REST controller that serve any request for captured Pokemon information
 *
 * @author augustoportjhin
 *
 */
@RestController
@RequestMapping(PokemonUserController.BASE_URL)
public class PokemonUserController {
    public static final String BASE_URL = "/rest/secure/inv/";

    @Autowired
    private PokemonUserDAO dao;
    @Autowired
    private PokemonDAO pokemonDao;

    private UserDTO currentUser() {
        // TODO: Implement security filter
        // Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // return authentication.getName();
        String username = PokemonConstant.TEST_USER;
        return new UserDTO(username, null);
    }

    /**
     * Delete a pokemon caught by curernt user
     *
     * @param pokemonUserId
     *            the pokemon-user id to be deleted
     */
    @DeleteMapping("id/{id}")
    public void delete(@NonNull @Validated @PathVariable("id") String pokemonUserId) {
        UserDTO user = currentUser();
        dao.delete(user, pokemonUserId);
    }

    /**
     * Get all Pokemons caught by current user
     *
     * @return List of Pokemons
     */
    @GetMapping("")
    public List<PokemonUserDTO> getAll() {
        UserDTO user = currentUser();
        return dao.getAll(user);
    }

    /**
     * Get specific pokemon that caught by current user
     *
     * @param pokemonUserId
     *            the pokemon to be returned
     * @param response
     *            the HTTP Response to enter HTTP response code
     * @return the pokemon if valid, HTTP 204 is not found
     */
    @GetMapping("id/{id}")
    public PokemonUserDTO getOne(@NonNull @Validated @PathVariable("id") String pokemonUserId, HttpServletResponse response) {
        UserDTO user = currentUser();
        PokemonUserDTO result = dao.get(user, pokemonUserId);
        if (result == null) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        return result;

    }

    /**
     * Register new pokemon caught by current user
     *
     * @param newDTO
     *            new Pokemon data
     * @param response
     *            the HTTP Response to enter HTTP response code
     * @return the newly pokemon and HTTP 201 if valid, HTTP 400 request is not valid
     */
    @PostMapping
    public PokemonUserDTO insert(@NonNull @RequestBody PokemonUserDTO newDTO, HttpServletResponse response) {
        UserDTO user = currentUser();

        Assert.notNull(newDTO.getPokemon(), "Pokemon required");
        PokemonDTO pokemonDTO = pokemonDao.get(newDTO.getPokemon().getId());
        Assert.notNull(pokemonDTO, "Invalid pokemon");

        // Fill up empty variable
        newDTO.setUserId(user.getId());
        newDTO.setCaughtDate(new Date());
        if (newDTO.getCurrentCP() == null) {
            newDTO.setCurrentCP(1);
        }
        if (newDTO.getCurrentStats() == null) {
            newDTO.setCurrentStats(pokemonDTO.getStats());
        }
        if (newDTO.getCustomName() == null) {
            newDTO.setCustomName(pokemonDTO.getName());
        }

        PokemonUserDTO result = dao.insert(newDTO);
        if (result != null) {
            response.setStatus(HttpStatus.CREATED.value());
        } else {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        return result;
    }

    /**
     * Update existing pokemon caught by current user detail.
     *
     * @param param
     *            new Pokemon data. Only these values allowed to be updated
     *            <ul>
     *            <li>customName</li>
     *            <li>currentCP</li>
     *            <li>favourited</li>
     *            <li>currentStats</li>
     *            </ul>
     * @param response
     *            the HTTP Response to enter HTTP response code
     * @return the newly pokemon and HTTP 201 if valid, HTTP 400 request is not valid
     */
    @PutMapping
    public PokemonUserDTO update(@NonNull @RequestBody PokemonUserDTO param, HttpServletResponse response) {
        UserDTO user = currentUser();

        Assert.notNull(param.getId(), "Pokemon required");
        PokemonUserDTO existing = dao.get(user, param.getId());
        if (existing == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        // Fill up updated variable
        if (param.getCustomName() != null) {
            existing.setCustomName(param.getCustomName());
        }
        if (param.getCurrentCP() != null && param.getCurrentCP().compareTo(existing.getCurrentCP()) > 0) {
            PokemonDTO pokemonDTO = pokemonDao.get(existing.getPokemon().getId());
            existing.setCurrentCP(Math.min(param.getCurrentCP(), pokemonDTO.getMaxCP()));
        }
        if (param.getFavourited() != null) {
            existing.setFavourited(param.getFavourited());
        }
        if (param.getCurrentStats() != null) {
            existing.setCurrentStats(param.getCurrentStats());
        }

        PokemonUserDTO result = dao.update(existing);
        response.setStatus(HttpStatus.CREATED.value());
        return result;
    }
}