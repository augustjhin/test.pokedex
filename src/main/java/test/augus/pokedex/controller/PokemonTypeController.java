package test.augus.pokedex.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.augus.pokedex.dto.master.BaseTypeDTO;
import test.augus.pokedex.model.dao.BaseTypeDAO;

/**
 * This is the REST controller that serve request for Pokemon type information
 *
 * @author augustoportjhin
 *
 */
@RestController
@RequestMapping(PokemonTypeController.BASE_URL)
public class PokemonTypeController {
    public static final String BASE_URL = "/rest/type/";

    @Autowired
    private BaseTypeDAO dao;

    /**
     * Get all Pokemon types registered in the system
     *
     * @return List of Pokemon types
     */
    @GetMapping
    public List<BaseTypeDTO> getAll() {
        return dao.getAll();
    }

    /**
     * Get one of type by ID
     *
     * @param id
     *            the Pokemon type ID. Must be alphanumeric and does not contains space.
     * @param response
     *            the response handler
     * @return Pokemon type entity if found, HTTP 204 if id not exists
     */
    @GetMapping("id/{id}")
    public BaseTypeDTO getOne(@NonNull @Validated @PathVariable("id") String id, HttpServletResponse response) {
        Assert.notNull(id, "Type ID is required");
        Assert.isTrue(id.matches("^[a-zA-Z0-9_]*$"), "Id contains special character");
        BaseTypeDTO result = dao.get(id);
        if (result == null) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        return result;
    }
}
