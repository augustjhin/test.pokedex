package test.augus.pokedex.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.dto.PokemonParamDTO;
import test.augus.pokedex.model.dao.PokemonDAO;

/**
 * This is the REST controller that serve any request for Pokedex/Pokemon information
 *
 * @author augustoportjhin
 *
 */
@RestController
@RequestMapping(PokedexController.BASE_URL)
public class PokedexController {
    public static final String BASE_URL = "/rest/pokemon/";
    @Autowired
    private PokemonDAO dao;

    /**
     * Get all Pokemons registered in the system
     *
     * @return List of Pokemons
     */
    @GetMapping("")
    public List<PokemonDTO> getAll(@RequestBody(required = false) PokemonParamDTO params) {
        if (params == null) {
            return dao.getAll();
        } else {
            return dao.getAll(params);
        }
    }

    /**
     * Get Pokemon(s) by type
     *
     * @param type
     *            the Type ID. Must be alphanumeric and does not contains space.
     * @return List of Pokemon entity
     */
    @GetMapping("type/{id}")
    public List<PokemonDTO> getByType(@NonNull @Validated @PathVariable("id") String type) {
        Assert.notNull(type, "Type is required");
        Assert.isTrue(type.matches("^[a-zA-Z0-9_]*$"), "Type contains special character");
        return dao.findByType(type);
    }

    /**
     * Get one of Pokemon by ID
     *
     * @param id
     *            the Pokemon ID. Must be alphanumeric and does not contains space.
     * @param response
     *            the response handler
     * @return Pokemon entity if found, HTTP 204 if id not exists
     */
    @GetMapping("id/{id}")
    public PokemonDTO getOne(@NonNull @Validated @PathVariable("id") String id, HttpServletResponse response) {
        Assert.notNull(id, "Pokemon ID is required");
        Assert.isTrue(id.matches("^[a-zA-Z0-9_]*$"), "Id contains special character");
        PokemonDTO result = dao.get(id);
        if (result == null) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        return result;
    }
}
