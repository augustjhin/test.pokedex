package test.augus.pokedex.internal.setup;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import test.augus.pokedex.dto.BasePokemonDTO;
import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.dto.PokemonUserDTO;
import test.augus.pokedex.dto.UserDTO;
import test.augus.pokedex.model.dao.PokemonDAO;
import test.augus.pokedex.model.dao.PokemonUserDAO;
import test.augus.pokedex.model.dao.UserDAO;

@Component
public class PokemonUserSetupBean implements ISetupBean {
    private static final Logger LOG = LoggerFactory.getLogger(PokemonUserSetupBean.class);

    @Override
    public void doSetup(ApplicationContext applicationContext) {
        setupUserPokemon(applicationContext);
    }

    private List<PokemonUserDTO> enrich(List<PokemonDTO> pokemons, final UserDTO user) {
        return pokemons.stream().map(p -> {
            PokemonUserDTO pu = new PokemonUserDTO();
            pu.setPokemon(new BasePokemonDTO(p.getId(), p.getName()));
            pu.setCurrentStats(p.getStats());
            pu.setUserId(user.getId());
            return pu;
        }).collect(Collectors.toList());
    }

    private List<PokemonUserDTO> getInitialData(ApplicationContext applicationContext) {
        UserDTO user = getUser("user1@test.com", applicationContext);
        List<PokemonDTO> pokemons = getPokemons(applicationContext);
        List<PokemonUserDTO> pokemonUserList = enrich(pokemons, user);
        return pokemonUserList;
    }

    private List<PokemonDTO> getPokemons(ApplicationContext applicationContext) {
        PokemonDAO dao = applicationContext.getBean(PokemonDAO.class);
        PokemonDTO pokemon = dao.get("PIKACHU");
        return Arrays.asList(pokemon);
    }

    private UserDTO getUser(String id, ApplicationContext applicationContext) {
        UserDAO dao = applicationContext.getBean(UserDAO.class);
        return dao.get(id);
    }

    public void setupUserPokemon(ApplicationContext applicationContext) {
        LOG.info("Loading user-pokemon to database...");
        List<PokemonUserDTO> pokemonUserList = getInitialData(applicationContext);
        PokemonUserDAO dao = applicationContext.getBean(PokemonUserDAO.class);
        dao.insertAll(pokemonUserList);
        LOG.info("Done loading user-pokemon.");
    }
}
