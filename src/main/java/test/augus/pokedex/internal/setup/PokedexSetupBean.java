package test.augus.pokedex.internal.setup;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import test.augus.pokedex.dto.PokemonDTO;
import test.augus.pokedex.dto.master.BaseTypeDTO;
import test.augus.pokedex.dto.master.BuddySizeDTO;
import test.augus.pokedex.dto.master.CinematicMoveDTO;
import test.augus.pokedex.dto.master.FamilyDTO;
import test.augus.pokedex.dto.master.QuickMoveDTO;
import test.augus.pokedex.dto.master.RarityDTO;
import test.augus.pokedex.dto.pokemon.attributes.EncounterMovementTypeDTO;
import test.augus.pokedex.model.dao.BaseTypeDAO;
import test.augus.pokedex.model.dao.BuddySizeDAO;
import test.augus.pokedex.model.dao.CinematicMovementDAO;
import test.augus.pokedex.model.dao.EncounterMovementTypeDAO;
import test.augus.pokedex.model.dao.FamilyDAO;
import test.augus.pokedex.model.dao.PokemonDAO;
import test.augus.pokedex.model.dao.QuickMovementDAO;
import test.augus.pokedex.model.dao.RarityDAO;

@Component
public class PokedexSetupBean implements ISetupBean {
    private static final Logger LOG = LoggerFactory.getLogger(PokedexSetupBean.class);

    @Override
    public void doSetup(ApplicationContext applicationContext) {
        loadPokedex(applicationContext);
    }

    public List<PokemonDTO> getPokemons() {
        File file = new File(getClass().getClassLoader().getResource("pokemon.json").getFile());
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            // Deserialize JSON file into Java object.
            return mapper.readValue(file, new TypeReference<List<PokemonDTO>>() {
            });
        } catch (IOException e) {
            LOG.error("Unable to retrieve pokemon.json", e);
            return Collections.emptyList();
        }
    }

    private void loadPokedex(ApplicationContext applicationContext) {
        LOG.info("Loading Pokedex to database...");
        List<PokemonDTO> pokedex = getPokemons();
        if (pokedex.isEmpty()) {
            return;
        }

        // Extract unique master entities
        setupBuddySizeMaster(pokedex, applicationContext);
        setupFamilyMaster(pokedex, applicationContext);
        setupPokemonBaseTypeMaster(pokedex, applicationContext);
        setupRarityMaster(pokedex, applicationContext);
        setupMovementMaster(pokedex, applicationContext);
        setupEncounterMovementMaster(pokedex, applicationContext);

        // Save extended entities
        savePokemons(pokedex, applicationContext);

        LOG.info("Done loading Pokedex");
    }

    private void savePokemons(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {
        PokemonDAO dao = applicationContext.getBean(PokemonDAO.class);
        dao.insertAll(pokedex);
    }

    private void setupBuddySizeMaster(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {
        Set<BuddySizeDTO> buddySizes = pokedex.stream().map(PokemonDTO::getBuddySize).collect(Collectors.toSet());
        buddySizes.remove(null);
        BuddySizeDAO dao = applicationContext.getBean(BuddySizeDAO.class);
        dao.insertAll(buddySizes);
    }

    private void setupEncounterMovementMaster(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {
        Set<EncounterMovementTypeDTO> types = pokedex.stream().map(p -> p.getEncounter().getMovementType()).collect(Collectors.toSet());
        types.remove(null);
        EncounterMovementTypeDAO dao = applicationContext.getBean(EncounterMovementTypeDAO.class);
        dao.insertAll(types);
    }

    private void setupFamilyMaster(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {
        Set<FamilyDTO> families = pokedex.stream().map(PokemonDTO::getFamily).collect(Collectors.toSet());
        families.remove(null);
        FamilyDAO dao = applicationContext.getBean(FamilyDAO.class);
        dao.insertAll(families);
    }

    private void setupMovementMaster(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {

        Set<CinematicMoveDTO> cinematicMoves = new HashSet<>();
        Set<QuickMoveDTO> quickMoves = new HashSet<>();
        for (PokemonDTO pokemon : pokedex) {
            cinematicMoves.addAll(Arrays.asList(pokemon.getCinematicMoves()));
            quickMoves.addAll(Arrays.asList(pokemon.getQuickMoves()));
        }
        cinematicMoves.remove(null);
        quickMoves.remove(null);
        QuickMovementDAO quickDao = applicationContext.getBean(QuickMovementDAO.class);
        quickDao.insertAll(quickMoves);

        CinematicMovementDAO cinematicDao = applicationContext.getBean(CinematicMovementDAO.class);
        cinematicDao.insertAll(cinematicMoves);
    }

    private void setupPokemonBaseTypeMaster(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {
        Set<BaseTypeDTO> types = pokedex.stream().flatMap(p -> Arrays.asList(p.getTypes()).stream()).collect(Collectors.toSet());
        types.remove(null);
        BaseTypeDAO dao = applicationContext.getBean(BaseTypeDAO.class);
        dao.insertAll(types);
    }

    private void setupRarityMaster(Collection<PokemonDTO> pokedex, ApplicationContext applicationContext) {
        Set<RarityDTO> rarities = pokedex.stream().map(PokemonDTO::getRarity).collect(Collectors.toSet());
        rarities.remove(null);
        RarityDAO dao = applicationContext.getBean(RarityDAO.class);
        dao.insertAll(rarities);
    }
}