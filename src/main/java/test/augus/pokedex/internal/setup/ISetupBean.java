package test.augus.pokedex.internal.setup;

import org.springframework.context.ApplicationContext;

public interface ISetupBean {
    void doSetup(ApplicationContext applicationContext);
}
