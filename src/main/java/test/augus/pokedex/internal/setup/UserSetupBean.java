package test.augus.pokedex.internal.setup;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import test.augus.pokedex.dto.UserDTO;
import test.augus.pokedex.model.dao.UserDAO;

@Component
public class UserSetupBean implements ISetupBean {
    private static final Logger LOG = LoggerFactory.getLogger(UserSetupBean.class);

    private List<UserDTO> createInitialUser() {
        return Arrays.asList( //
                new UserDTO("user1@test.com", "user1"), //
                new UserDTO("user2@test.com", "user2") //
        );
    }

    @Override
    public void doSetup(ApplicationContext applicationContext) {
        setupUsers(applicationContext);
    }

    public void setupUsers(ApplicationContext applicationContext) {
        LOG.info("Loading users to database...");
        UserDAO dao = applicationContext.getBean(UserDAO.class);
        dao.insertAll(createInitialUser());
        LOG.info("Done loading users.");
    }
}
