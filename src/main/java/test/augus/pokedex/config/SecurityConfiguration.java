package test.augus.pokedex.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

//@Configuration
//@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication() //
                .withUser("user1@test.com").password(passwordEncoder().encode("p@ssw0rd")).roles("USER").and() //
                .withUser("user2@test.com").password(passwordEncoder().encode("p@ssw0rd")).roles("USER");
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http //
                .csrf().disable() //
                .authorizeRequests() //
                .antMatchers("/admin/**").hasRole("ADMIN") //
                .antMatchers("/rest/secure/**").hasAnyRole("USER", "ADMIN") //
                .antMatchers("/rest/pokemon*").anonymous() //
                .antMatchers("/rest/login*").permitAll() //
                .antMatchers("/rest/logout*").permitAll() //
                .anyRequest().authenticated() //
                .and() //
                .formLogin() //
                .loginProcessingUrl("/perform_login") //
                .and() //
                .logout() //
                .logoutUrl("/perform_logout") //
                .deleteCookies("JSESSIONID") //
                .logoutSuccessHandler(new SimpleUrlLogoutSuccessHandler()); //
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}